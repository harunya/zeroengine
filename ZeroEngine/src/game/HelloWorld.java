package game;

import engine.Attrib;
import engine.Camera;
import engine.CameraA;
import engine.CubeTexture;
import engine.KeyHandler;
import engine.RelativePoints;
import engine.Shader;
import engine.ShaderProgram;
import engine.Texture;
import engine.TextureAtlas;
import engine.UVs;
import engine.VAO;
import engine.VBO;
import engine.Window;
import engine.gameObjects.Block;
import engine.gameObjects.Blocks;
import engine.light.DirectionLight;
import engine.light.Lamp;
import engine.light.LightManager;
import engine.light.PointLight;
import engine.materials.GrassMaterial;
import engine.materials.ObsidianMaterial;
import engine.materials.VoidMaterial;
import engine.shapes.Chunk;
import static engine.shapes.Chunk.BLOCK_SIZE;
import engine.shapes.Cube;
import engine.shapes.Mesh;
import engine.shapes.Model;
import engine.shapes.Pyramid;
import static engine.shapes.Shape.Shape;
import engine.shapes.SkyBox;
import engine.shapes.Triangle;
import static engine.shapes.Triangle.Triangle;
import engine.terrain.GenerationRule;
import engine.terrain.SimpleChunksGenerator;
import engine.terrain.SimplexNoise;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import org.lwjgl.*;
import org.lwjgl.glfw.*;
import org.lwjgl.opengl.*;
import org.lwjgl.system.*;

import java.nio.*;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.joml.Matrix4f;
import org.joml.Vector3f;
import org.joml.Vector3i;

import static org.lwjgl.glfw.Callbacks.*;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import org.lwjgl.stb.STBImage;
import static org.lwjgl.system.MemoryStack.*;
import static org.lwjgl.system.MemoryUtil.*;



public class HelloWorld {
        static long lastFrame = 0;
        
        public static long getTime() {
            return System.nanoTime() / 1000000;
        }
        
        public static int getDelta() {
            long time = getTime();
            int delta = (int) (time - lastFrame);
            lastFrame = time;

            return delta;
        }
        
        public static float valueOrMin(float v, float min){
            if(v < min) return min;
            
            return v;
        }
        
        public static float valueOrMax(float v, float max){
            if(v > max) return max;
            
            return v;
        }

	public static void main(String[] args) throws Exception {
            
            TextureAtlas ta = new TextureAtlas();
            ta.gen("./res/block/");
            
            
            Window win = new Window("Game", 1280, 800);
            
            ta.sendTextureToOGL();
            
            
            glEnable(GL_CULL_FACE);
            glFrontFace(GL_CCW);
            
            CameraA cam = new CameraA(win.getID());
                
            
            win.addKeyHandler(new KeyHandler(87){
                public void onKey(long w, int key, int sc, int a, int ms){
                    cam.up();
                }
            });
            
            win.addKeyHandler(new KeyHandler(83){
                public void onKey(long w, int key, int sc, int a, int ms){
                    cam.down();
                }
            });
            
            win.addKeyHandler(new KeyHandler(65){
                public void onKey(long w, int key, int sc, int a, int ms){
                    cam.left();
                }
            });
            
            win.addKeyHandler(new KeyHandler(68){
                public void onKey(long w, int key, int sc, int a, int ms){
                    cam.right();
                }
            });
            
            
            
            ShaderProgram smWithNoLight = new ShaderProgram();
            Shader shdn = new Shader(GL20.GL_FRAGMENT_SHADER);
            shdn.create("res/shaders/shader2.fs");
            smWithNoLight.attachShader(shdn);
            
            Shader shdn2 = new Shader(GL20.GL_VERTEX_SHADER);
            shdn2.create("res/shaders/shader2.vs");
            smWithNoLight.attachShader(shdn2);

            smWithNoLight.link();
            
            ShaderProgram sm = new ShaderProgram();
            
            Shader shd = new Shader(GL20.GL_FRAGMENT_SHADER);
            shd.create("res/shaders/shader.fs");
            sm.attachShader(shd);
            
            Shader shd2 = new Shader(GL20.GL_VERTEX_SHADER);
            shd2.create("res/shaders/shader.vs");
            sm.attachShader(shd2);

            sm.link();
            
            
            Texture skyBoxTexture = Texture.createTexture("res/skybox.jpg");
            
            Mesh skyBox = new SkyBox(-100f,-100f,-100f, 200f, skyBoxTexture);
            skyBox.setUV(UVs.skybox_1);
            
            //INITIALISATING BLOCKS
            Blocks.register(new Block().setTexture(ta, "block2.png"));
            Blocks.register(new Block().setTexture(ta, "stone.png"));
           
            // GENERATION CHUNKS 
            SimpleChunksGenerator generator = new SimpleChunksGenerator(1); // 1 - seed
            generator.textureAtlas = ta;
            
            // Adding rules
            generator.addRule(new GenerationRule(){
                public Block use(SimpleChunksGenerator g){
                    if(g.curPointer.y == g.genPointer.y) // Grass
                        return Blocks.getBlock(0);  
                    
                    if(g.curPointer.y < g.genPointer.y) //Stone
                        return Blocks.getBlock(1);  
                    
                    return null;  
                }
            }); 
            
            // SimpleChunksGenerator::gen(size_x, size_y, size_z, persistance, scale)
            generator.gen(5, 2, 5, 0.3F, 0.030F); 
            
            
            LightManager lightManager = new LightManager();
            
            
            DirectionLight sunLight = new DirectionLight(new Vector3f(0,0,0),
                                                         new Vector3f(0.1f,0.1f,0.1f),
                                                         new Vector3f(0.2f,0.2f,0.2f),
                                                         new Vector3f(1f,1f,1f));
            
            
            lightManager.setDirectionLight(sunLight);
            lightManager.setAmbientPower(1f);
            
            
            
            win.addKeyHandler(new KeyHandler(32){
                @Override
                public void onKey(long w, int key, int sc, int a, int ms){
                    PointLight pointLight1 = new PointLight(new Vector3f(cam.getPosVec().x,cam.getPosVec().y,cam.getPosVec().z),
                                                    new Vector3f(1f,1f,1f),
                                                    new Vector3f(1f,1f,1f),
                                                    new Vector3f(1f,1f,1f),
                                                    1.0f, 0.0014f, 0.000007f);
                    
                    lightManager.addPointLight(pointLight1);
                }
            });
            
            
            Matrix4f proj =  (new Matrix4f()).perspective( 90.0f, (float)win.getWidth()/(float)win.getHeight(), 0.1f, 300.0f);
            
            
            int fps = 0, dfps = 0;
            float gameTime = 0;
            long time = System.currentTimeMillis();
            while(win.work()){
                if(System.currentTimeMillis()-time < 1000) fps++;
                else{ dfps = fps; fps = 0; time = System.currentTimeMillis(); }
                
                Vector3f viewPos = cam.getPosVec();
                Matrix4f view = cam.getViewMatrix();
                Vector3f camFront = cam.getFront();
                
                // DRAWING SKYBOX
                smWithNoLight.use();
                lightManager.sendData(smWithNoLight);
                
                win.setTitle("Game [FPS: "+dfps+"] {Time: "+gameTime+"} "+cam.getPosVec());
                
                smWithNoLight.sendDataUniform("projection", proj);
                smWithNoLight.sendDataUniform("view", view);
                
                skyBox.x(viewPos.x-100f);
                skyBox.y(viewPos.y-100f);
                skyBox.z(viewPos.z-100f);
                
                
                skyBox.draw(smWithNoLight);
                
                // DRAWING MAP
                sm.use();
                
                sm.sendDataUniform("projection", proj);
                sm.sendDataUniform("view", view);
                
                sm.sendDataUniform("viewPos", new float[]{viewPos.x, viewPos.y, viewPos.z});
                
                
                for(Chunk chunk: generator.getChunks())
                    chunk.draw(sm);
                
                lightManager.sendData(sm);
                
                
                System.gc();
                win.pushUpdates();
            }
            
            win.destroy();
	}

}