/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package engine;

import engine.shapes.Vertex;
import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;
import java.util.Arrays;
import org.joml.Vector3f;
import org.lwjgl.BufferUtils;

/**
 *
 * @author mineStile
 */
public class RelativePoints {
    float[] points;
    private int sizePoint;
    private int size;
    
    public RelativePoints(float[] points, int sizePoint){
        this.points = points;
        this.size = points.length;
        this.sizePoint = sizePoint;
    }

    public RelativePoints(Vertex[] vertices, int sizePoint) {
        float[] p = new float[vertices.length*3*2*3];
        for(int i=0, t=0; t < vertices.length; t++){
            p[i++] = vertices[t].position.x;
            p[i++] = vertices[t].position.y;
            p[i++] = vertices[t].position.z;
            
            p[i++] = vertices[t].normal.x;
            p[i++] = vertices[t].normal.y;
            p[i++] = vertices[t].normal.z;
            
            p[i++] = vertices[t].texCoords.x;
            p[i++] = vertices[t].texCoords.y;
        }
        this.points = p;
        
        //System.out.println(Arrays.toString(p));
        
        this.sizePoint = sizePoint;
    }

    public RelativePoints(Vertex[] vertices, Vector3f vecPos, Vector3f size, int sizePoint) {
        float[] p = new float[vertices.length*3*2*3];
        for(int i=0, t=0; t < vertices.length; t++){
                
            p[i++] = vertices[t].position.x*size.x+vecPos.x;
            p[i++] = vertices[t].position.y*size.y+vecPos.y;
            p[i++] = vertices[t].position.z*size.z+vecPos.z;
            
            p[i++] = vertices[t].normal.x;
            p[i++] = vertices[t].normal.y;
            p[i++] = vertices[t].normal.z;
            
            p[i++] = vertices[t].texCoords.x;
            p[i++] = vertices[t].texCoords.y;
        }
        this.points = p;
        
        //System.out.println(Arrays.toString(p));
        
        this.sizePoint = sizePoint;
    }
    
    public float[] getPoints(){ return points; }
    
    public int getDimension(){ return sizePoint; }
    
    public FloatBuffer getBuffer(){
        FloatBuffer buffer = BufferUtils.createFloatBuffer(points.length);

        buffer.put(points);
        buffer.flip();
        
        
        return buffer;
    }
    
    public RelativePoints resize(float x, float y, float z){
        float[] points = this.getPoints();
        for(int i=0;i<points.length;i+=3){
            points[i] = points[i]*x;
            points[i+1] = points[i+1]*y;
            points[i+2] = points[i+2]*z;
        }
        return this;
    }

    public int getSize() {
        return this.size;
    }
}
