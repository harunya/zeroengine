/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package engine;

import org.lwjgl.glfw.*;
import org.lwjgl.opengl.*;
import org.lwjgl.system.*;

import java.io.*;
import java.nio.*;
import java.util.*;

import static java.lang.Math.*;
import org.lwjgl.BufferUtils;

import static org.lwjgl.glfw.Callbacks.*;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL12.*;
import static org.lwjgl.stb.STBImage.*;
import static org.lwjgl.stb.STBImageResize.*;
import static org.lwjgl.system.MemoryStack.*;
import static org.lwjgl.system.MemoryUtil.*;
/**
 *
 * @author mineStile
 */
public class Texture {
    private Image im;
    private int id;
    private int vboID;
    
    public int getId(){return this.id;}
    public int getVBOID(){return this.vboID;}
    
    public void toGL(float[] texCoords){
        FloatBuffer buffer = BufferUtils.createFloatBuffer(texCoords.length);
        buffer.put(texCoords);
        buffer.flip();
        
        this.vboID = GL15.glGenBuffers();
        
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, this.vboID);
        GL15.glBufferData(GL15.GL_ARRAY_BUFFER, buffer, GL15.GL_STATIC_DRAW);
        GL20.glVertexAttribPointer(1, 2, GL11.GL_FLOAT, false, 0, 0);
        GL20.glEnableVertexAttribArray(1);
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
        
        buffer.clear();
    }
    
    private static void premultiplyAlpha(Image im) {
        int stride = im.w * 4;
        for (int y = 0; y < im.h; y++) {
            for (int x = 0; x < im.w; x++) {
                int i = y * stride + x * 4;

                float alpha = (im.image.get(i + 3) & 0xFF) / 255.0f;
                im.image.put(i + 0, (byte)round(((im.image.get(i + 0) & 0xFF) * alpha)));
                im.image.put(i + 1, (byte)round(((im.image.get(i + 1) & 0xFF) * alpha)));
                im.image.put(i + 2, (byte)round(((im.image.get(i + 2) & 0xFF) * alpha)));
            }
        }
    }
    
    public static Texture createTexture(String path) {
        return createTexture(new Image(path));
    }
    
    public static Texture createTexture(Image im) {
        
        int texID = glGenTextures();

        glBindTexture(GL_TEXTURE_2D, texID);
        //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,  GL15.GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL15.GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

        
        int format;
        if (im.comp == 3) {
            if ((im.w & 3) != 0) {
                glPixelStorei(GL_UNPACK_ALIGNMENT, 2 - (im.w & 1));
            }
            format = GL_RGB;
        } else {
            premultiplyAlpha(im);

            glEnable(GL_BLEND);
            glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);

            format = GL_RGBA;
        }

        glTexImage2D(GL_TEXTURE_2D, 0, format, im.w, im.h, 0, format, GL_UNSIGNED_BYTE, im.image);

        ByteBuffer input_pixels = im.image;
        int        input_w      = im.w;
        int        input_h      = im.h;
        int        mipmapLevel  = 0;
        while (1 < input_w || 1 < input_h) {
            int output_w = Math.max(1, input_w >> 1);
            int output_h = Math.max(1, input_h >> 1);

            ByteBuffer output_pixels = memAlloc(output_w * output_h * im.comp);
                stbir_resize_uint8_generic(
                input_pixels, input_w, input_h, input_w * im.comp,
                output_pixels, output_w, output_h, output_w * im.comp,
                im.comp, im.comp == 4 ? 3 : STBIR_ALPHA_CHANNEL_NONE, STBIR_FLAG_ALPHA_PREMULTIPLIED,
                STBIR_EDGE_CLAMP,
                STBIR_FILTER_MITCHELL,
                STBIR_COLORSPACE_SRGB
            );

            if (mipmapLevel == 0) {
                stbi_image_free(im.image);
            } else {
                memFree(input_pixels);
            }

            glTexImage2D(GL_TEXTURE_2D, ++mipmapLevel, format, output_w, output_h, 0, format, GL_UNSIGNED_BYTE, output_pixels);

            input_pixels = output_pixels;
            input_w = output_w;
            input_h = output_h;
        }
        if (mipmapLevel == 0) {
            stbi_image_free(im.image);
        } else {
            memFree(input_pixels);
        }
        
        Texture tex = new Texture();
        
        tex.id = texID;
        tex.im = im;
        

        return tex;
    }
}
