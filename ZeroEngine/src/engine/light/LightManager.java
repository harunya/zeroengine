/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package engine.light;

import engine.ShaderProgram;
import org.joml.Vector3f;
import org.joml.Vector4f;

/**
 *
 * @author mineStile
 */
public class LightManager {
    private DirectionLight directionLight = null;
    private PointLight[] pointLights = new PointLight[0];
    private float fogDensity = 0f;
    private Vector4f ambientConfig = new Vector4f(1f,1f,1f, 1f);
    
    
    public void setFogDensity(float fogDensity){
        this.fogDensity = fogDensity;
    }
    
    public void setAmbientColor(Vector3f clr){
        this.ambientConfig.x = clr.x;
        this.ambientConfig.y = clr.y;
        this.ambientConfig.z = clr.z;
    }
    
    public void setAmbientPower(float power){
        this.ambientConfig.w = power;
    }
    
    
    public void addPointLight(PointLight p){
        PointLight[] tmp = new PointLight[pointLights.length + 1];
        
        for(int i=0; i < pointLights.length; i++)
            tmp[i] = pointLights[i];
        
        tmp[pointLights.length] = p;
        
        this.pointLights = tmp;
        
    }
    
    public void setDirectionLight(DirectionLight d){
        this.directionLight = d;
    }
    
    public void sendData(ShaderProgram sm){
        sm.sendDataUniform("ambientConfig",  new float[]{ambientConfig.x, ambientConfig.y, ambientConfig.z, ambientConfig.w});
        
        sm.sendDataUniform("density", this.fogDensity);
        
        if(this.directionLight != null)
            this.directionLight.sendData(sm);
        
        PointLight.sendDatas(sm, pointLights);
        
        //sm.sendDataUniform("material.ambient", new float[]{ this.directionLight.getAmbient().x, this.directionLight.getAmbient().y, this.directionLight.getAmbient().z });
    }
}
