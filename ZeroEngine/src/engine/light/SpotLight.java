/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package engine.light;

import engine.ShaderProgram;
import org.joml.Vector3f;

/**
 *
 * @author mineStile
 */
public class SpotLight {
    private Vector3f ambient;
    private Vector3f specular;
    private Vector3f diffuse;
    private Vector3f position;
    
    private final float constant;
    private final float linear;
    private final float quadratic;
    
    public SpotLight(Vector3f position, Vector3f ambient, Vector3f specular, Vector3f diffuse, float constant, float linear, float quadratic){
        this.position = position;
        this.ambient = ambient;
        this.diffuse = diffuse;
        this.specular = specular;
        
        this.constant = constant;
        this.linear = linear;
        this.quadratic = quadratic;
    }
    
    public void sendData(ShaderProgram sm){
        sm.sendDataUniform("POINT_LIGHTS",  1);
        
        sm.sendDataUniform("pointLights[0].position",  new float[]{position.x, position.y, position.z});
        sm.sendDataUniform("pointLights[0].ambient",  new float[]{ambient.x, ambient.y, ambient.z});
        sm.sendDataUniform("pointLights[0].diffuse",  new float[]{diffuse.x, diffuse.y, diffuse.z}); // darken the light a bit to fit the scene
        sm.sendDataUniform("pointLights[0].specular", new float[]{specular.x, specular.y, specular.z}); 
        sm.sendDataUniform("pointLights[0].constant",  constant);
        sm.sendDataUniform("pointLights[0].linear",    linear);
        sm.sendDataUniform("pointLights[0].quadratic", quadratic);
    }
    
    public static void sendDatas(ShaderProgram sm, SpotLight[] lights){
        sm.sendDataUniform("POINT_LIGHTS", lights.length);
        
        for(int i=0; i < lights.length; i++){
            sm.sendDataUniform("pointLights["+i+"].position",  new float[]{lights[i].position.x, lights[i].position.y, lights[i].position.z});
            sm.sendDataUniform("pointLights["+i+"].ambient",  new float[]{lights[i].ambient.x, lights[i].ambient.y, lights[i].ambient.z});
            sm.sendDataUniform("pointLights["+i+"].diffuse",  new float[]{lights[i].diffuse.x, lights[i].diffuse.y, lights[i].diffuse.z}); // darken the light a bit to fit the scene
            sm.sendDataUniform("pointLights["+i+"].specular", new float[]{lights[i].specular.x, lights[i].specular.y, lights[i].specular.z}); 
            sm.sendDataUniform("pointLights["+i+"].constant",  lights[i].constant);
            sm.sendDataUniform("pointLights["+i+"].linear",    lights[i].linear);
            sm.sendDataUniform("pointLights["+i+"].quadratic", lights[i].quadratic);
        }
        
        
        
    }
    
    public Vector3f getAmbient(){ return this.ambient; }
    public Vector3f getSpecular(){ return this.specular; }
    public Vector3f getDiffuse(){ return this.diffuse; }
    public Vector3f getPosition(){ return this.position; }
    
    public void setAmbient(Vector3f a){ this.ambient = a; }
    public void setSpecular(Vector3f a){ this.specular = a; }
    public void setDiffuse(Vector3f a){ this.diffuse = a; }

}
