/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package engine.light;

import engine.ShaderProgram;
import engine.shapes.Cube;
import engine.shapes.Mesh;
import org.joml.Vector3f;

/**
 *
 * @author mineStile
 */
public class Lamp {
    private PointLight pl;
    private Mesh object;
    private LightManager lightManager = new LightManager();
    
    public Lamp(PointLight pl){
        this.pl = pl;
        this.object = new Cube(pl.getPosition().x, pl.getPosition().y, pl.getPosition().z, 0.1f);
        
        this.lightManager.addPointLight(this.pl);
    }
    
    public Lamp(Vector3f pos){
        this.pl = pl;
        this.object = new Cube(pos.x, pos.y, pos.z, 0.1f);
        
        this.lightManager.addPointLight(this.pl);
    }
    
    public Lamp(Vector3f position, Vector3f ambient, Vector3f specular, Vector3f diffuse, float constant, float linear, float quadratic){
        this.pl = new PointLight(position, ambient, specular, diffuse, constant, linear, quadratic);
        this.object = new Cube(position.x, position.y, position.z, 0.1f);
        
        this.lightManager.addPointLight(this.pl);
    }
    
    public Lamp(Vector3f position, Vector3f ambient){
        this.pl = new PointLight(position, ambient, new Vector3f(1f,1f,1f), new Vector3f(1f,1f,1f), 1.0f, 0.09f, 0.032f);
        this.object = new Cube(position.x, position.y, position.z, 0.1f);
        
        this.lightManager.addPointLight(this.pl);
    }
    
    public Lamp(Vector3f position, Vector3f ambient, Vector3f specular, Vector3f diffuse){
        this.pl = new PointLight(position, ambient, specular, diffuse, 1.0f, 0.09f, 0.032f);
        this.object = new Cube(position.x, position.y, position.z, 0.1f);
        
        this.lightManager.addPointLight(this.pl);
    }
    
    public void setMesh(Mesh m){
        this.object = m;
    }
    public Mesh getMesh(){
        return this.object;
    }
    
    
    public void draw(ShaderProgram sm){
        this.lightManager.sendData(sm);
        this.object.draw(sm);
    }
    
    public void setManager(LightManager lm){
        this.lightManager = lm;
        this.lightManager.addPointLight(this.pl);
    }
    
}
