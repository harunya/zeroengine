/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package engine.terrain;

import engine.TextureAtlas;
import engine.UVs;
import engine.gameObjects.Block;
import engine.shapes.Chunk;
import engine.shapes.Cube;
import engine.shapes.Mesh;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.joml.Vector3i;

/**
 *
 * @author mineStile
 */
public class SimpleChunksGenerator {
    
    public TextureAtlas textureAtlas;
    private SimplexNoise noise;
    private List<Chunk> chunks = new ArrayList<>(); 
    private List<GenerationRule> rules = new ArrayList<>(); 
    
    public Vector3i size; 
    public Vector3i curPointer; 
    public Vector3i genPointer;
    
    public SimpleChunksGenerator(int seed){
        noise = new SimplexNoise(seed);
    }
    
    public List<Chunk> getChunks(){
        return this.chunks;
    }
    
    public void gen(int chunks_x, int chunks_y, int chunks_z, float persistance, float scale){

        this.size = new Vector3i(chunks_x, chunks_y, chunks_z);
        
        chunks = new ArrayList<>(); 
        
        BufferedImage bi = noise.generateHeightmap(256, chunks_y*Chunk.SIZE, persistance, scale, 0, chunks_y*Chunk.SIZE);

        for(int x=0;x<chunks_x;x++){
            for(int y=0;y<chunks_y;y++){
                for(int z=0;z<chunks_z;z++){

                    Chunk chunk = new Chunk(x, y, z);
                    chunk.textureAtlas = textureAtlas;

                    Block[] meshes = new Block[Chunk.SIZE * Chunk.SIZE * Chunk.SIZE];
                    for(int ox=0, ic=0;ox<Chunk.SIZE;ox++){
                        for(int oy=0;oy<Chunk.SIZE;oy++){
                            for(int oz=0;oz<Chunk.SIZE;oz++,ic++){
                                Color mycolor = new Color(bi.getRGB(ox+x*Chunk.SIZE, oz+z*Chunk.SIZE));

                                int oyf = mycolor.getRed() % Chunk.SIZE; // Local y block in chunk
                                int yf = ((int)(mycolor.getRed()/Chunk.SIZE)); //Global y chunk
                                
                                Block m = new Block(new Vector3i(ox, oy, oz));
                                
                                m.mesh = null;
                                
                                //m.setPosition(new Vector3i(ox, oy, oz));
                                
                                //m.setTexture(textureAtlas, "stone.png");
                                
                                this.curPointer = new Vector3i(x*Chunk.SIZE+ox*(int)Chunk.BLOCK_SIZE, y*Chunk.SIZE + oy, z*Chunk.SIZE+oz*(int)Chunk.BLOCK_SIZE);
                                this.genPointer = new Vector3i(this.curPointer.x, yf*Chunk.SIZE + oyf, this.curPointer.z);
                                
                                for(GenerationRule rule: this.rules){
                                    if(rule.genY == -1 || rule.genY == yf*Chunk.SIZE + oyf){
                                        Block b = rule.use(this);
                                        if(b != null){
                                            //m.setTexture(textureAtlas, b.mesh.textureName);
                                            m.mesh = b.mesh;
                                            
                                            m.mesh.x(m.position.x);
                                            m.mesh.y(m.position.y);
                                            m.mesh.z(m.position.z);
                                        }
                                        
                                            
                                    }
                                    
                                }
                                
                                
                                
                                meshes[ic] = m;
                            }
                        }

                    }
                    try {
                        
                        chunk.add(meshes);
                    } catch (Exception ex) {
                        Logger.getLogger(SimpleChunksGenerator.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                    chunk.rebuild();
                    
                    Block b = new Block(new Vector3i(7,7,7));
                    
                    b.setTexture(textureAtlas, "lamp.png");
                    
                    //chunk.setBlockAt(b);
                    
                    chunks.add(chunk);
                }
            }
        }
    }

    public void addRule(GenerationRule generationRule) {
        this.rules.add(generationRule);
    }
}
