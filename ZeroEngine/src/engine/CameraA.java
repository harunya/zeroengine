package engine;

import org.joml.Matrix4f;
import org.joml.Vector3f;
import static org.lwjgl.glfw.GLFW.GLFW_CURSOR;
import static org.lwjgl.glfw.GLFW.GLFW_CURSOR_DISABLED;
import static org.lwjgl.glfw.GLFW.glfwSetCursorPosCallback;
import static org.lwjgl.glfw.GLFW.glfwSetInputMode;


public class CameraA {
 
    private float lastX = 400; // Координаты мыши
    private float lastY = 300;// Координаты мыши
    private float pitch = 0.0f; //Угол
    private float yaw   = -90.0f; //Угол
    private float speed   = 0.3f; 
    boolean firstMouse = true;
 
    Vector3f cameraFront = new Vector3f(0f,0f,-1f);
    Vector3f up = new Vector3f(0f,1f,0f);
    Vector3f cameraPos = new Vector3f(0f,0f,3f);
 
    public CameraA(long window){
        glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
        
        glfwSetCursorPosCallback(window, (window1, xpos, ypos) -> {
            if(firstMouse) { // Это чтобы курсор не скакал при первом движении
                lastX = (float) xpos;
                lastY = (float) ypos;
                firstMouse = false;
            }
            float xoffset = (float) (xpos - lastX);
            float yoffset = (float) (lastY - ypos);
 
            lastX = (float) xpos;
            lastY = (float) ypos;
 
            float sensitivity = 0.07f;
            xoffset *= sensitivity;
            yoffset *= sensitivity;
            pitch += yoffset;
            yaw   += xoffset;
            if(pitch > 89.0f) pitch =  89.0f; // Чтобы мы не могли разворачиваться на тучу градусов
            if(pitch < -89.0f) pitch = -89.0f;
            Vector3f front = new Vector3f((float) ( Math.cos(Math.toRadians(yaw)) * Math.cos(Math.toRadians(pitch)) ),
                                          (float) Math.sin(Math.toRadians(pitch)),
                                          (float) ( Math.sin(Math.toRadians(yaw)) * Math.cos(Math.toRadians(pitch)) ));
            cameraFront = front.normalize();
        });
    }
 
    public Vector3f getFront(){
        return this.cameraFront;
    }
    public Matrix4f getViewMatrix(){
        Vector3f up = new Vector3f(0f,1f,0f);
        return (new Matrix4f()).lookAt(cameraPos, cameraPos.add(cameraFront, new Vector3f()), up);
    }
    
    public Vector3f getPosVec(){
        return cameraPos;
    }
 
    public void up(){
        cameraPos = cameraPos.add(cameraFront.mul(speed, new Vector3f()));
    }
 
    public void down(){
        cameraPos = cameraPos.sub(cameraFront.mul(speed, new Vector3f()));
    }
 
    public void left(){
        cameraPos = cameraPos.sub(cameraFront.cross(up, new Vector3f()).normalize().mul(speed, new Vector3f()));
    }
 
    public void right(){
        cameraPos = cameraPos.add(cameraFront.cross(up, new Vector3f()).normalize().mul(speed, new Vector3f()));
    }
 
 
}