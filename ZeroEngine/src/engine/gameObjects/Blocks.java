/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package engine.gameObjects;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mineStile
 */
public class Blocks{
    private static List<Block> blocks = new ArrayList<>();
    
    public static void register(Block b){
        blocks.add(b);
    }
    
    public static List<Block> getBlocks(){
        return blocks;
    }
    
    public static Block getBlock(int ind){
        Block res = new Block();
        res.mesh = blocks.get(ind).mesh;
        
        return new Block(blocks.get(ind));
    }
}
