/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package engine.gameObjects;

import engine.TextureAtlas;
import engine.UVs;
import engine.shapes.Chunk;
import engine.shapes.Cube;
import engine.shapes.Mesh;
import org.joml.Vector3f;
import org.joml.Vector3i;

/**
 *
 * @author mineStile
 */
public class Block implements Cloneable{
    public Mesh mesh;
    public Vector3i position;
    
    public Block(Vector3i pos){
        this.mesh = new Cube(pos.x,pos.y,pos.z, Chunk.BLOCK_SIZE);
        this.position = pos;
    }
    
    public Block(Block b){
        this.mesh = new Cube(b.position.x,b.position.y,b.position.z, Chunk.BLOCK_SIZE);
        this.mesh.setPreparedUV(b.mesh.getTexture(), b.mesh.textureName, b.mesh.getUV());
        this.mesh.material(b.mesh.material());
        
        this.position = new Vector3i(b.position.x, b.position.y, b.position.z);
    }
    
    public Block(){
        this(new Vector3i(0,0,0));
    }
    
    
    public Block setTexture(TextureAtlas textureAtlas, String fn, float[] uv){
        this.mesh.setUV(textureAtlas, fn, uv);
        
        return this;
    }
    
    public Block setTexture(TextureAtlas textureAtlas, String fn){
        this.mesh.setUV(textureAtlas, fn, UVs.cube);
        
        return this;
    }
    
    public void setMesh(Mesh m){
        this.mesh = m;
    }

    public Block setPosition(Vector3i pos) {
        this.mesh.x(pos.x);
        this.mesh.y(pos.y);
        this.mesh.z(pos.z);
        this.position = pos;
        return this;
    }
    
    public Object clone() throws CloneNotSupportedException { 
        return super.clone(); 
    } 
}
