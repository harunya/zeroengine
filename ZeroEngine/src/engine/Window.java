/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package engine;


import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;
import static org.lwjgl.glfw.Callbacks.glfwFreeCallbacks;
import static org.lwjgl.glfw.GLFW.GLFW_FALSE;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_ESCAPE;
import static org.lwjgl.glfw.GLFW.GLFW_OPENGL_ANY_PROFILE;
import static org.lwjgl.glfw.GLFW.GLFW_OPENGL_PROFILE;
import static org.lwjgl.glfw.GLFW.GLFW_RELEASE;
import static org.lwjgl.glfw.GLFW.GLFW_RESIZABLE;
import static org.lwjgl.glfw.GLFW.GLFW_TRUE;
import static org.lwjgl.glfw.GLFW.GLFW_VISIBLE;
import static org.lwjgl.glfw.GLFW.glfwCreateWindow;
import static org.lwjgl.glfw.GLFW.glfwDefaultWindowHints;
import static org.lwjgl.glfw.GLFW.glfwDestroyWindow;
import static org.lwjgl.glfw.GLFW.glfwGetPrimaryMonitor;
import static org.lwjgl.glfw.GLFW.glfwGetVideoMode;
import static org.lwjgl.glfw.GLFW.glfwGetWindowSize;
import static org.lwjgl.glfw.GLFW.glfwInit;
import static org.lwjgl.glfw.GLFW.glfwMakeContextCurrent;
import static org.lwjgl.glfw.GLFW.glfwPollEvents;
import static org.lwjgl.glfw.GLFW.glfwSetErrorCallback;
import static org.lwjgl.glfw.GLFW.glfwSetKeyCallback;
import static org.lwjgl.glfw.GLFW.glfwSetWindowPos;
import static org.lwjgl.glfw.GLFW.glfwSetWindowShouldClose;
import static org.lwjgl.glfw.GLFW.glfwSetWindowTitle;
import static org.lwjgl.glfw.GLFW.glfwShowWindow;
import static org.lwjgl.glfw.GLFW.glfwSwapBuffers;
import static org.lwjgl.glfw.GLFW.glfwSwapInterval;
import static org.lwjgl.glfw.GLFW.glfwTerminate;
import static org.lwjgl.glfw.GLFW.glfwWindowHint;
import static org.lwjgl.glfw.GLFW.glfwWindowShouldClose;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.opengl.GL;
import org.lwjgl.opengl.GL11;
import static org.lwjgl.opengl.GL11.GL_DEPTH_TEST;
import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.opengl.GL11.glFlush;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL30;
import org.lwjgl.system.MemoryStack;
import static org.lwjgl.system.MemoryStack.stackPush;
import static org.lwjgl.system.MemoryUtil.NULL;

/**
 *
 * @author mineStile
 */
public class Window {
    private String winTitle;
    private int width, height;
    private long window;
    private List<KeyHandler> keyHandlers = new ArrayList();
    
    public Window(String title, int w, int h){
        this.height = h;
        this.width = w;
        this.winTitle = title;
        
        init();
    }
    
    public boolean work(){
        if(this.is_close())
            return false;
        
        
       GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
        
        return true;
    }
    
    public void pushUpdates(){
        //glFlush();
        glfwSwapBuffers(window);
	glfwPollEvents();
    }
    
    public boolean is_close(){ return glfwWindowShouldClose(window); }
    
    public void destroy(){
        glfwFreeCallbacks(window);
	glfwDestroyWindow(window);

	glfwTerminate();
	glfwSetErrorCallback(null).free();
    }
    
    private void init() {
        GLFWErrorCallback.createPrint(System.err).set();

        if ( !glfwInit() )
                throw new IllegalStateException("Unable to initialize GLFW");


        glfwDefaultWindowHints();
        glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);
        glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_ANY_PROFILE);

        GLFWErrorCallback.createPrint(System.err).set();


        window = glfwCreateWindow(width, height, winTitle, NULL, NULL);
        if ( window == NULL )
                throw new RuntimeException("Failed to create the GLFW window");


        glfwSetKeyCallback(window, (window, key, scancode, action, mods) -> {
                if ( key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE )
                        glfwSetWindowShouldClose(window, true);
                
                for(KeyHandler handler : this.keyHandlers)
                    if(handler.key == key)
                        handler.onKey(window, key, scancode, action, mods);
                
        });

        try ( MemoryStack stack = stackPush() ) {
                IntBuffer pWidth = stack.mallocInt(1);
                IntBuffer pHeight = stack.mallocInt(1);


                glfwGetWindowSize(window, pWidth, pHeight);


                GLFWVidMode vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());


                glfwSetWindowPos(
                        window,
                        (vidmode.width() - pWidth.get(0)) / 2,
                        (vidmode.height() - pHeight.get(0)) / 2
                );
        }

        glfwMakeContextCurrent(window);

        glfwSwapInterval(1);


        glfwShowWindow(window);

        GL.createCapabilities();
        
        glEnable(GL_DEPTH_TEST);
        glEnable(GL11.GL_NEAREST);
    }
    
    public void setTitle(String title){
        glfwSetWindowTitle(this.window, title);
    }

    public void checkErrors() {
        int error = GL11.glGetError();
        if(error != GL11.GL_NO_ERROR){
            System.out.println("Err: "+error);
            System.exit(0);
        }
    }
    
    public void addKeyHandler(KeyHandler e){
        this.keyHandlers.add(e);
    }

    public long getID() {
        return this.window;
    }
    
    public double getWidth() {
        return this.width;
    }
    
    public double getHeight() {
        return this.height;
    }
    
    public void draw(int id, int type){
        //System.out.println(vbo.getId());
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, id);
            GL11.glDrawArrays(type, 0, 18);
        GL30.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
    }
    
    public void draw(VBO vbo, int type){
    }
    
    public void draw(VBO[] vbos, int type){
        //System.out.println(vbo.getId());
        int[] elementsIds = new int[vbos.length];
        int[] elementsCounts = new int[vbos.length];
        for(int i=0;i<elementsIds.length;i++){
            //elementsIds[i] = vbos[i].id;
            //elementsCounts[i] = vbos[i].vertices;
        }
        
        //GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vbo.getId());
            GL15.glMultiDrawArrays(type, elementsIds, elementsCounts);
        //GL30.glBindVertexArray(0);
    }
    
    public void draw(List<VBO> vbos, int type){
        //System.out.println(vbo.getId());
        int[] elementsIds = new int[vbos.size()];
        int[] elementsCounts = new int[vbos.size()];
        for(int i=0;i<vbos.size();i++){
           // elementsIds[i] = vbos.get(i).id;
            //elementsCounts[i] = vbos.get(i).vertices;
        }
        
        //GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vbo.getId());
            GL15.glMultiDrawArrays(type, elementsIds, elementsCounts);
        //GL30.glBindVertexArray(0);
    }
}
