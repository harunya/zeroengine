/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package engine;

import static engine.IOUtil.ioResourceToByteBuffer;
import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.DataBufferInt;
import java.awt.image.DataBufferShort;
import java.awt.image.DataBufferUShort;
import org.lwjgl.system.*;

import java.io.*;
import java.nio.*;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import org.lwjgl.BufferUtils;

import static org.lwjgl.stb.STBImage.*;
import static org.lwjgl.system.MemoryStack.*;
/**
 *
 * @author mineStile
 */
public class Image {
    public ByteBuffer image;
    
    private String fileName;

    public int w;
    public int h;
    public int comp;

    public long window;
    public int  ww;
    public int  wh;

    public static ByteBuffer convertImage(BufferedImage image){     
        int[] pixels = new int[image.getWidth() * image.getHeight()];
        image.getRGB(0, 0, image.getWidth(), image.getHeight(), pixels, 0, image.getWidth());

        ByteBuffer buffer = BufferUtils.createByteBuffer(image.getWidth() * image.getHeight() * 8);

        for(int y = 0; y < image.getHeight(); y++)
        {
            for(int x = 0; x < image.getWidth(); x++)
            {
                int pixel = pixels[y * image.getWidth() + x];
                buffer.put((byte) ((pixel >> 16) & 0xFF));     // Red component
                buffer.put((byte) ((pixel >> 8) & 0xFF));      // Green component
                buffer.put((byte) (pixel & 0xFF));               // Blue component
                buffer.put((byte) ((pixel >> 24) & 0xFF));    // Alpha component. Only for RGBA
            }
        }

        buffer.flip();

        return buffer;
    }
    
    public static ByteBuffer bufferedImageToBuffer(BufferedImage originalImage){
        try {
            byte[] imageInByte;

            //Create a ByteArrayOutputStrea object to write image to
            ByteArrayOutputStream baos = new ByteArrayOutputStream();

            //Write the image to the OutputStream
            ImageIO.write(originalImage, "jpg", baos);

            baos.flush();

            //Initialise the byte array object with the image that was written to the OutputStream
            imageInByte = baos.toByteArray();
            baos.close();
               
            return ByteBuffer.wrap(imageInByte);
            
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    
    private ByteBuffer imageToByteBuffer(BufferedImage bi){
        ByteBuffer byteBuffer;
        DataBuffer dataBuffer = bi.getRaster().getDataBuffer();

        if (dataBuffer instanceof DataBufferByte) {
            byte[] pixelData = ((DataBufferByte) dataBuffer).getData();
            byteBuffer = ByteBuffer.wrap(pixelData);
        }
        else if (dataBuffer instanceof DataBufferUShort) {
            short[] pixelData = ((DataBufferUShort) dataBuffer).getData();
            byteBuffer = ByteBuffer.allocate(pixelData.length * 2);
            byteBuffer.asShortBuffer().put(ShortBuffer.wrap(pixelData));
        }
        else if (dataBuffer instanceof DataBufferShort) {
            short[] pixelData = ((DataBufferShort) dataBuffer).getData();
            byteBuffer = ByteBuffer.allocate(pixelData.length * 2);
            byteBuffer.asShortBuffer().put(ShortBuffer.wrap(pixelData));
        }
        else if (dataBuffer instanceof DataBufferInt) {
            int[] pixelData = ((DataBufferInt) dataBuffer).getData();
            byteBuffer = ByteBuffer.allocate(pixelData.length * 4);
            byteBuffer.asIntBuffer().put(IntBuffer.wrap(pixelData));
        }
        else {
            throw new IllegalArgumentException("Not implemented for data buffer type: " + dataBuffer.getClass());
        }
        return byteBuffer;
    }
    
    public Image(BufferedImage image) {
        try {
            File outputfile = new File("temp.png");
            ImageIO.write(image, "png", outputfile);
            
            ByteBuffer imageBuffer = null;
            try {
                imageBuffer = ioResourceToByteBuffer("temp.png", 8 * 1024);
                //imageBuffer = imageToByteBuffer(image);
            } catch (Exception ex) {
                Logger.getLogger(Image.class.getName()).log(Level.SEVERE, null, ex);
            }

            try (MemoryStack stack = stackPush()) {
                IntBuffer w    = stack.mallocInt(1);
                IntBuffer h    = stack.mallocInt(1);
                IntBuffer comp = stack.mallocInt(1);

                // Decode the image
                this.image = stbi_load_from_memory(imageBuffer, w, h, comp, 0);
                if (image == null) {
                    throw new RuntimeException("Failed to load image: " + stbi_failure_reason());
                }

                this.w = w.get(0);
                this.h = h.get(0);
                this.comp = comp.get(0);
            }
        } catch (IOException ex) {
            Logger.getLogger(Image.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public Image(String imagePath) {
        
        ByteBuffer imageBuffer = null;
        try {
            imageBuffer = ioResourceToByteBuffer(imagePath, 8 * 1024);
        } catch (Exception ex) {
            System.out.println("Error for load: "+imagePath);
            Logger.getLogger(Image.class.getName()).log(Level.SEVERE, null, ex);
        }

        try (MemoryStack stack = stackPush()) {
            IntBuffer w    = stack.mallocInt(1);
            IntBuffer h    = stack.mallocInt(1);
            IntBuffer comp = stack.mallocInt(1);

            // Decode the image
            this.image = stbi_load_from_memory(imageBuffer, w, h, comp, 0);
            if (image == null) {
                throw new RuntimeException("Failed to load image: " + stbi_failure_reason());
            }

            this.w = w.get(0);
            this.h = h.get(0);
            this.comp = comp.get(0);
        }
    }

}
