/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package engine;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;

/**
 *
 * @author mineStile
 */
public class Shader {
    private String source;
    private int type;
    private int id;
    private List<Attrib> attribs = new ArrayList();
    
    public Shader(int type){
        this.type = type;
        
        this.init();
    }
    
    public void addAttrib(Attrib a){
        this.attribs.add(a);
    }
    
    public void create(String filename){
        this.load(filename);
        
        GL20.glShaderSource(this.id, this.source);
        GL20.glCompileShader(this.id);

        if(!GL20.glGetShaderInfoLog(this.id).equals("")){
            System.out.println(GL20.glGetShaderInfoLog(this.id));

            System.exit(0);
        }
     
    }
    
    private void init(){
        id = GL20.glCreateShader(this.type);
    }
    
    public boolean load(String filename){
        try{
            try(BufferedReader br = new BufferedReader(new FileReader(filename))) {
                StringBuilder sb = new StringBuilder();
                String line = br.readLine();

                while (line != null) {
                    sb.append(line);
                    sb.append(System.lineSeparator());
                    line = br.readLine();
                }
                this.source = sb.toString();
            }
        }catch (IOException e){
            e.printStackTrace();
            return false;
        }

        return true;
    }
    
    public int getId(){ return this.id; }
    
    public List<Attrib> getAttribs(){ return this.attribs; }
}
