/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package engine.materials;

import engine.Texture;
import org.joml.Vector4f;

/**
 *
 * @author mineStile
 */
public interface Material {

    public static Vector4f DEFAULT_COLOUR = new Vector4f(1f);
    void Material();
    
    float[] getAmbient();
    float[] getDiffuse();
    float[] getSpecular();
    Texture getTexture();
    void setTexture(Texture t);
    float getShininess();
}
