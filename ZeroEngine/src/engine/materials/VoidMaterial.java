/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package engine.materials;

import engine.Texture;

/**
 *
 * @author mineStile
 */
public class VoidMaterial implements Material {

    @Override
    public void Material() {
        
    }

    @Override
    public float[] getAmbient() {
        return new float[]{0f,0f,0f, 1f};
    }

    @Override
    public float[] getDiffuse() {
        return new float[]{0f,0f,0f, 1f};
    }

    @Override
    public float[] getSpecular() {
        return new float[]{0f,0f,0f, 1f};
    }

    @Override
    public float getShininess() {
        return 32f;
    }
    
    @Override
    public Texture getTexture() {
        return new Texture();
    }

    @Override
    public void setTexture(Texture t) {
        
    }
}
