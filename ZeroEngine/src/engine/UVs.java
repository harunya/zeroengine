/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package engine;

import java.util.Arrays;
import org.joml.Vector2f;
import org.joml.Vector2i;
import org.joml.Vector4f;
import org.joml.Vector4i;

/**
 *
 * @author mineStile
 */
public class UVs {
    /*
    
        atlas.x - width atlas image
        atlas.y - height atlas image
    
        img.x - position x image on atlas image
        img.y - position y image on atlas image
        img.z - width image on atlas image
        img.w - height image on atlas image
    
    */
    public static float[] getPreparedUV(float[] unprUV, Vector2f atlas, Vector4f img){
        float[] prep = new float[unprUV.length];
        for(int i=0; i<prep.length; i++){
            if(i % 2 == 0){ // x
                prep[i] = img.x / atlas.x + ((float) ((unprUV[i] * img.z) / atlas.x));
            }else{ // y
                prep[i] = img.y / atlas.y + ((float) ((unprUV[i] * img.w) / atlas.y));
            }
        }
        return prep;
    }
    
    public static float[] cubeUnpr = new float[]{
                1,    0,
                1,    1,
                0,    1,
                1,    0,
                0,    1,
                0,    0,
                
                2,    0,
                2,    1,
                1,    1,
                2,    0,
                1,    1,
                1,    0,
                
                3,    0,
                3,    1,
                2,    1,
                3,    0,
                2,    1,
                2,    0,
                
                
                1,    1,
                1,    2,
                0,    2,
                1,    1,
                0,    2,
                0,    1,
                
                2,    1,
                2,    2,
                1,    2,
                2,    1,
                1,    2,
                1,    1,
                
                3,    1,
                3,    2,
                2,    2,
                3,    1,
                2,    2,
                2,    1,
            };
    
    
    public static float[] cube = new float[]{
                (1/3f)*1,    (1/2f)*0,
                (1/3f)*1,    (1/2f)*1,
                (1/3f)*0,    (1/2f)*1,
                (1/3f)*1,    (1/2f)*0,
                (1/3f)*0,    (1/2f)*1,
                (1/3f)*0,    (1/2f)*0,
                
                (1/3f)*2,    (1/2f)*0,
                (1/3f)*2,    (1/2f)*1,
                (1/3f)*1,    (1/2f)*1,
                (1/3f)*2,    (1/2f)*0,
                (1/3f)*1,    (1/2f)*1,
                (1/3f)*1,    (1/2f)*0,
                
                (1/3f)*3,    (1/2f)*0,
                (1/3f)*3,    (1/2f)*1,
                (1/3f)*2,    (1/2f)*1,
                (1/3f)*3,    (1/2f)*0,
                (1/3f)*2,    (1/2f)*1,
                (1/3f)*2,    (1/2f)*0,
                
                
                (1/3f)*1,    (1/2f)*1,
                (1/3f)*1,    (1/2f)*2,
                (1/3f)*0,    (1/2f)*2,
                (1/3f)*1,    (1/2f)*1,
                (1/3f)*0,    (1/2f)*2,
                (1/3f)*0,    (1/2f)*1,
                
                (1/3f)*2,    (1/2f)*1,
                (1/3f)*2,    (1/2f)*2,
                (1/3f)*1,    (1/2f)*2,
                (1/3f)*2,    (1/2f)*1,
                (1/3f)*1,    (1/2f)*2,
                (1/3f)*1,    (1/2f)*1,
                
                (1/3f)*3,    (1/2f)*1,
                (1/3f)*3,    (1/2f)*2,
                (1/3f)*2,    (1/2f)*2,
                (1/3f)*3,    (1/2f)*1,
                (1/3f)*2,    (1/2f)*2,
                (1/3f)*2,    (1/2f)*1,
            };
    public static float[] skybox_0 = new float[] {
                
                (1/4f)*3, (1/3f)*2,
                (1/4f)*2, (1/3f)*2,
                (1/4f)*3, (1/3f)*1,
                (1/4f)*2, (1/3f)*1,
                (1/4f)*2, (1/3f)*2,
                (1/4f)*3, (1/3f)*1,
                
                (1/4f)*0, (1/3f)*2,
                (1/4f)*1, (1/3f)*2,
                (1/4f)*0, (1/3f)*1,
                (1/4f)*1, (1/3f)*1,
                (1/4f)*1, (1/3f)*2,
                (1/4f)*0, (1/3f)*1,
                
                (1/4f)*3, (1/3f)*2,
                (1/4f)*4, (1/3f)*2,
                (1/4f)*3, (1/3f)*1,
                (1/4f)*4, (1/3f)*1,
                (1/4f)*4, (1/3f)*2,
                (1/4f)*3, (1/3f)*1,
                
                (1/4f)*2, (1/3f)*2,
                (1/4f)*1, (1/3f)*2,
                (1/4f)*2, (1/3f)*1,
                (1/4f)*1, (1/3f)*1,
                (1/4f)*1, (1/3f)*2,
                (1/4f)*2, (1/3f)*1,
                
                (1/4f)*3, (1/3f)*2,
                (1/4f)*3, (1/3f)*3,
                (1/4f)*4, (1/3f)*2,
                (1/4f)*4, (1/3f)*3,
                (1/4f)*3, (1/3f)*3,
                (1/4f)*4, (1/3f)*2,
                

                (1/4f)*3, (1/3f)*1,
                (1/4f)*3, (1/3f)*0,
                (1/4f)*4, (1/3f)*1,
                (1/4f)*4, (1/3f)*0,
                (1/4f)*3, (1/3f)*0,
                (1/4f)*4, (1/3f)*1,
                
            };
    
    public static float[] skybox_1 = new float[] {
                
                (1/4f)*3, (1/3f)*1,
                (1/4f)*2, (1/3f)*2,
                (1/4f)*3, (1/3f)*2,
                (1/4f)*3, (1/3f)*1,
                (1/4f)*2, (1/3f)*1,
                (1/4f)*2, (1/3f)*2,
                
                (1/4f)*1, (1/3f)*1,
                (1/4f)*0, (1/3f)*2,
                (1/4f)*1, (1/3f)*2,
                (1/4f)*1, (1/3f)*1,
                (1/4f)*0, (1/3f)*1,
                (1/4f)*0, (1/3f)*2,
                
                (1/4f)*2, (1/3f)*1,
                (1/4f)*1, (1/3f)*2,
                (1/4f)*2, (1/3f)*2,
                (1/4f)*2, (1/3f)*1,
                (1/4f)*1, (1/3f)*1,
                (1/4f)*1, (1/3f)*2,
                
                
                (1/4f)*4, (1/3f)*1,
                (1/4f)*3, (1/3f)*2,
                (1/4f)*4, (1/3f)*2,
                (1/4f)*4, (1/3f)*1,
                (1/4f)*3, (1/3f)*1,
                (1/4f)*3, (1/3f)*2,
                
                
                (1/4f)*2, (1/3f)*3,
                (1/4f)*1, (1/3f)*2,
                (1/4f)*1, (1/3f)*3,
                (1/4f)*2, (1/3f)*3,
                (1/4f)*2, (1/3f)*2,
                (1/4f)*1, (1/3f)*2,
                
                
                (1/4f)*1, (1/3f)*0,
                (1/4f)*2, (1/3f)*1,
                (1/4f)*2, (1/3f)*0,
                (1/4f)*1, (1/3f)*0,
                (1/4f)*1, (1/3f)*1,
                (1/4f)*2, (1/3f)*1,
                
                
//                (1/4f)*2, (1/3f)*0,
//                (1/4f)*2, (1/3f)*1,
//                (1/4f)*1, (1/3f)*1,
//                (1/4f)*2, (1/3f)*0,
//                (1/4f)*1, (1/3f)*1,
//                (1/4f)*1, (1/3f)*0,
                
            };
}
