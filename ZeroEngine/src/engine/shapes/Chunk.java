/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package engine.shapes;

import engine.RelativePoints;
import engine.ShaderProgram;
import engine.Texture;
import engine.TextureAtlas;
import engine.UVs;
import engine.VBO;
import engine.gameObjects.Block;
import engine.terrain.SimplexNoise;
import java.awt.image.BufferedImage;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.joml.Matrix4f;
import org.joml.Vector3f;
import org.joml.Vector3i;

/**
 *
 * @author mineStile
 */
public class Chunk {
    public static final int SIZE = 8;
    public static final float BLOCK_SIZE = 1f;
    
    private float ox = 0, oy = 0, oz = 0;
    private Mesh mesh = new Mesh();
    private Block[][][] cubes = new Block[SIZE][SIZE][SIZE];
    
    public TextureAtlas textureAtlas;

    public Chunk(int x, int y, int z) {
        this.ox = x;
        this.oy = y;
        this.oz = z;
    }
    
    public void optimize(){
        this.mesh.optimize();
    }
    
    public void add(Block[] c) throws java.lang.Exception{
        
        for(Block cube: c){
            if(cube == null )
                throw Exception("Placed incorrect Block in Chunk("+ox+","+oy+","+oz+") with null");
            
            if(cube.position.x >= SIZE || cube.position.y >= SIZE || cube.position.z >= SIZE || cube.position.x < 0 || cube.position.y < 0 || cube.position.z < 0 )
                throw Exception("Placed on incorrect place in Chunk("+ox+","+oy+","+oz+") with Block("+cube.position.x+", "+cube.position.y+", "+cube.position.z+")");
            
            this.cubes[cube.position.x][cube.position.y][cube.position.z] = cube;
        }
        
        //this.rebuild(); -- StackOverflow
    }
    
    public void add(Mesh[] m){
        float[] points = new float[0];
        
        if(mesh.vbo() != null){
            points = mesh.vbo().getPoints().getPoints();
            //points = mesh.
        }
        
        
        
        int countv = 0, countuv = 0;
        for(Mesh msh: m){
            if(msh != null)
                countv += msh.vbo().getPoints().getPoints().length;
        }
        
        float[] newPoints = new float[points.length + countv];
        //float[] newUV = new float[points.length + countuv];
        
        //System.out.println(points.length);
        
        int i=0;
        for(;i < points.length; i++)
            newPoints[i] = points[i];
        
        for(Mesh msh: m){
            if(msh == null)
                continue;
            
            float[] points2 = msh.vbo().getPoints().getPoints();
            //Matrix4f model = msh.getModelMatrix();
            
            for(int t=0; t < points2.length; t++, i++){
                //sssif(t % 6 == 0 || (t-1) % 6 == 0)
                    newPoints[i] = (float) points2[t];
            }
        }
        
        this.mesh = new Mesh();
        
        //mesh.r(0.3f);
        //mesh.g(0.7f);
        //mesh.b(0.9f);
        
        mesh.x(ox*Chunk.SIZE);
        mesh.y(oy*Chunk.SIZE);
        mesh.z(oz*Chunk.SIZE);
        
        mesh.setTexture(this.textureAtlas.getTexture());
        
        
        //mesh.setUV(textureAtlas, "block2.png", UVs.cube);
        
        //mesh.w(m.length * 0.1f);
        //mesh.h(m.length * 0.1f);
        //mesh.l(m.length * 0.1f);
        
        this.mesh.setVBO(new VBO(new RelativePoints(newPoints, 3)));
        
        //System.out.println(mesh.vbo().getPoints().getPoints().length);
    }
    
    public void rebuild(){
        Mesh[] meshes = new Mesh[SIZE * SIZE * SIZE];
        
        for(int i=0, k=0; i<SIZE; i++)
            for(int j=0; j<SIZE; j++)
                for(int l=0; l<SIZE; l++, k++){
                    Block b = this.cubes[i][j][l];
                    
                    if(b == null){
                        b = new Block(new Vector3i(i, j, l));
                    }
                    
                    
                    
                    meshes[k] = b.mesh;
                }
                
        this.add(meshes);
    }
    
    public Block getBlockAt(Vector3i pos){
        //if()
        return this.cubes[pos.x][pos.y][pos.z];
    }
    
    public void setBlockAt(Block b){
        this.cubes[b.position.x][b.position.y][b.position.z] = b;
        
        this.rebuild();
    }
    
    public void draw(ShaderProgram sm){
        //this.rebuild();
        mesh.draw(sm);
    }
    
    public void gen(long seed){
        //Random rand = new Random(seed);
        
        
        
        Mesh[] meshes = new Mesh[Chunk.SIZE * Chunk.SIZE * Chunk.SIZE];
        
        
        int c = 0;
        for(int x=0; x < Chunk.SIZE; x++){
            for(int y=0; y < Chunk.SIZE; y++){
                for(int z=0; z < Chunk.SIZE; z++, c++){
                    Mesh m = new Cube(x*BLOCK_SIZE,y*BLOCK_SIZE,z*BLOCK_SIZE, BLOCK_SIZE);
                    
                    m.setUV(textureAtlas, "stone.png", UVs.cube);
                    
                    //System.out.println((int)(Math.sin(x)*10));
                   //if((Math.sin(x)+1)*3.5 < y-0.01 && (Math.sin(x)+1)*3.5 > y+0.01)
                    //    m = null;
                    
                    
                    meshes[c] = m;
                }
            }
        }
        
         this.add(meshes);
    }

    private Exception Exception(String string) {
        throw new UnsupportedOperationException(string); //To change body of generated methods, choose Tools | Templates.
    }
    
}
