/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package engine.shapes;

import engine.EBO;
import engine.RelativePoints;
import engine.ShaderProgram;
import engine.Texture;
import engine.materials.CustomMaterial;
import engine.materials.GrassMaterial;
import engine.materials.Material;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;
import org.joml.Matrix4f;
import org.joml.Vector2f;
import org.joml.Vector3f;
import org.joml.Vector4f;
import org.lwjgl.PointerBuffer;
import org.lwjgl.assimp.AIColor4D;
import org.lwjgl.assimp.AIFace;
import org.lwjgl.assimp.AIMaterial;
import org.lwjgl.assimp.AIMesh;
import org.lwjgl.assimp.AINode;
import org.lwjgl.assimp.AIScene;
import org.lwjgl.assimp.AIString;
import org.lwjgl.assimp.AIVector2D;
import org.lwjgl.assimp.AIVector3D;
import org.lwjgl.assimp.Assimp;
import static org.lwjgl.assimp.Assimp.aiTextureType_DIFFUSE;

/**
 *
 * @author mineStile
 */
public class Model {
    /*  Данные модели  */
    List<Mesh> meshes;
    String directory;
    
    
    public Model(String path) throws Exception{
        meshes = loadModel(path);
            
        
    }
    public void draw(ShaderProgram s){
        for(Mesh m: meshes)
            m.draw(s);
    }
    
    public void setPosition(Vector3f p){
        for(Mesh m: meshes){
            m.x(p.x);
            m.y(p.y);
            m.z(p.z);
        }
    }
    
    public void setTexture(Texture t){
        for(Mesh m: meshes)
            m.setTexture(t);
        
    }
    public void setUV(float[] UV){
        for(Mesh m: meshes)
            m.setUV(UV);
        
    }
    
    public void setRGB(Vector3f rgb){
        for(Mesh m: meshes){
            m.r(rgb.x);
            m.g(rgb.y);
            m.b(rgb.z);
        }
            
        
    }
    
    public void setAmbient(Vector3f rgb){
        for(Mesh m: meshes){
            Material mat = m.material();
            m.material(new CustomMaterial(rgb, new Vector3f(mat.getDiffuse()[0], mat.getDiffuse()[1], mat.getDiffuse()[2]), new Vector3f(mat.getSpecular()[0], mat.getSpecular()[1], mat.getSpecular()[2]), mat.getShininess()));
        }
    }
    
    
    
    
    /*  Методы   */
    private List<Mesh> loadModel(String path) throws Exception{
        AIScene scene = Assimp.aiImportFile​(path, Assimp.aiProcess_Triangulate | Assimp.aiProcess_FlipUVs | Assimp.aiProcess_GenNormals);	

        if(scene == null) {
            throw new Exception("Error loading model");
        }
        
        directory = path.substring(0, path.lastIndexOf('/'));

        return processNode(scene.mRootNode(), scene);
    }
    private List<Mesh> processNode(AINode node, AIScene scene) throws Exception{
        int numMaterials = scene.mNumMaterials();
        PointerBuffer aiMaterials = scene.mMaterials();
        List<Material> materials = new ArrayList<>();
        for (int i = 0; i < numMaterials; i++) {
            AIMaterial aiMaterial = AIMaterial.create(aiMaterials.get(i));
            processMaterial(aiMaterial, materials, directory);
        }

        int numMeshes = scene.mNumMeshes();
        PointerBuffer aiMeshes = scene.mMeshes();
        List<Mesh> meshes = new ArrayList<>();
        for (int i = 0; i < numMeshes; i++) {
            AIMesh aiMesh = AIMesh.create(aiMeshes.get(i));
            Mesh mesh = processMesh(aiMesh, materials);
            meshes.add(mesh);
        }
        
        return meshes;
    }
    
    private static void processMaterial(AIMaterial aiMaterial, List<Material> materials, String texturesDir) throws Exception {
        AIColor4D colour = AIColor4D.create();

        AIString path = AIString.calloc();
        Assimp.aiGetMaterialTexture(aiMaterial, aiTextureType_DIFFUSE, 0, path, (IntBuffer) null, null, null, null, null, null);
        String textPath = path.dataString();
        Texture texture = null;
        if (textPath != null && textPath.length() > 0) {
            texture = Texture.createTexture(texturesDir + "/" + textPath);
        }

        Vector4f ambient = Material.DEFAULT_COLOUR;
        int result = Assimp.aiGetMaterialColor(aiMaterial, Assimp.AI_MATKEY_COLOR_AMBIENT, Assimp.aiTextureType_NONE, 0, colour);
        if (result == 0) {
            ambient = new Vector4f(colour.r(), colour.g(), colour.b(), colour.a());
        }

        Vector4f diffuse = Material.DEFAULT_COLOUR;
        result = Assimp.aiGetMaterialColor(aiMaterial, Assimp.AI_MATKEY_COLOR_DIFFUSE, Assimp.aiTextureType_NONE, 0, colour);
        if (result == 0) {
            diffuse = new Vector4f(colour.r(), colour.g(), colour.b(), colour.a());
        }

        Vector4f specular = Material.DEFAULT_COLOUR;
        result = Assimp.aiGetMaterialColor(aiMaterial, Assimp.AI_MATKEY_COLOR_SPECULAR, Assimp.aiTextureType_NONE, 0, colour);
        if (result == 0) {
            specular = new Vector4f(colour.r(), colour.g(), colour.b(), colour.a());
        }
        //System.out.println(ambient);
        //if(ambient.x() <= 0.03f && ambient.y() <= 0.03f && ambient.z() <= 0.03f)
        //    ambient = new Vector4f(0.2f, 0.2f, 0.2f, 1f);
        
        //if(diffuse.x() <= 0.03f && diffuse.y() <= 0.03f && diffuse.z() <= 0.03f)
        //    diffuse = new Vector4f(0.2f, 0.2f, 0.2f, 1f);
        
        //if(specular.x() <= 0.03f && specular.y() <= 0.03f && specular.z() <= 0.03f)
        //    diffuse = new Vector4f(0.4f, 0.5f, 0.3f, 1f);
        
        
        Material material = new CustomMaterial(ambient, diffuse, specular, 1.0f);
        material.setTexture(texture);
        materials.add(material);
    }
    private Mesh processMesh(AIMesh aiMesh, List<Material> materials){
        List<Vertex> vertices = new ArrayList<>();
        List<Integer> indices = new ArrayList<>();
        
        //for(int i=0; aiMesh.mNumFaces()
        
        //Buffer b = new Buffer();
        //Buffer g = aiMesh.m;
        AIVector3D.Buffer aiVertices = aiMesh.mVertices();
        AIVector3D.Buffer aiNormals = aiMesh.mNormals();
        AIFace.Buffer aiFaces = aiMesh.mFaces();
        AIVector3D.Buffer aiTextureCoords = aiMesh.mTextureCoords(0);
        
//        while(aiFaces.remaining() > 0){
//            AIFace f = aiFaces.get();
//            IntBuffer inds = f.mIndices();
//            
//            while(inds.remaining() > 0)
//                indices.add(inds.get());
//            
//        }

        processIndices(aiMesh, indices);
        
        //System.out.println(aiVertices.remaining()+" "+aiNormals.remaining()+" "+aiTextureCoords.remaining());
        
        int i=0;
        while (aiVertices.remaining() > 0) {
            Vertex vert = new Vertex();
            
            AIVector3D aiVertex = aiVertices.get();
            vert.position = new Vector3f(aiVertex.x(), aiVertex.y(), aiVertex.z());
            
            if(aiNormals != null){
                AIVector3D aiNormal = aiNormals.get();
                vert.normal = new Vector3f(aiNormal.x(), aiNormal.y(), aiNormal.z());
            }else vert.normal = new Vector3f(0);
            
            if(aiTextureCoords != null){
                AIVector3D aiTexCoords = aiTextureCoords.get();
                vert.texCoords = new Vector2f(aiTexCoords.x(), aiTexCoords.y());
            }else vert.texCoords = new Vector2f(0);

            vertices.add(vert);
            
            i++;
        }
        
        
        
        //processIndices(aiMesh, indices);

        Mesh mesh = new Mesh();
        
        
        Material material;
        int materialIdx = aiMesh.mMaterialIndex();
        if (materialIdx >= 0 && materialIdx < materials.size()) {
            material = materials.get(materialIdx);
        } else {
            material = new GrassMaterial();
        }
        mesh.setMaterial(material);
        mesh.setTexture(material.getTexture());
        
        mesh.loadModel(Mesh.vertexToArray(vertices), Mesh.listToArray(indices));

        return mesh;

        
    }
    
    private static void processIndices(AIMesh aiMesh, List<Integer> indices) {
        int numFaces = aiMesh.mNumFaces();
        AIFace.Buffer aiFaces = aiMesh.mFaces();
        for (int i = 0; i < numFaces; i++) {
          AIFace aiFace = aiFaces.get(i);
          IntBuffer buffer = aiFace.mIndices();
          while (buffer.remaining() > 0) {
              int t = buffer.get();
                indices.add(t);
          }
        }
      }
    //private List<Texture> loadMaterialTextures(AIMaterial mat, int type, String typeName){
    //    
    //}

    public void setSize(Vector3f p) {
        for(Mesh m: meshes){
            m.w(p.x);
            m.h(p.y);
            m.l(p.z);
        }
    }
}
