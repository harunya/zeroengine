/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package engine.shapes;

import org.joml.Vector2f;
import org.joml.Vector3f;

/**
 *
 * @author mineStile
 */
public class Vertex {
    public Vector3f position;
    public Vector3f normal;
    public Vector2f texCoords;
    
    public Vertex(){
        
    }
    
    public Vertex(Vector3f position, Vector3f normal, Vector2f texCoords){
        this.position = position;
        this.normal = normal;
        this.texCoords = texCoords;
    }
}
