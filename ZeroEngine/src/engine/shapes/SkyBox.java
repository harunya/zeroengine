/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package engine.shapes;

import engine.RelativePoints;
import engine.Texture;
import engine.VBO;
import org.joml.Matrix4f;
import org.joml.Vector2f;
import org.joml.Vector3f;

/**
 *
 * @author mineStile
 */
public class SkyBox extends Mesh {
    @Override
    public void reloadModel(){
        model = new Matrix4f();

        model.translate(new Vector3f((float)this.x(), (float)this.y(), (float)this.z()));
        model.scale(new Vector3f((float)this.w(), (float)this.h(), (float)this.l()));
    }
    
    
    @Override
    public void reloadVBO(){                     //V1     N1      V2      N2       V3     N3  
        float[] points = new float[]{            0,0,0, 0,0,1,   1,0,0, 0,0,1,   0,1,0, 0,0,1,     
                                                 1,1,0, 0,0,1,   1,0,0, 0,0,1,   0,1,0, 0,0,1,     //Forward
                                                 0,0,1, 0,0,-1,  1,0,1, 0,0,-1,  0,1,1, 0,0,-1,
                                                 1,1,1, 0,0,-1,  1,0,1, 0,0,-1,  0,1,1, 0,0,-1,    //Backward
                                                 0,0,0, 0,-1,0,  0,0,1, 0,-1,0,  0,1,0, 0,-1,0,
                                                 0,1,1, 0,-1,0,  0,0,1, 0,-1,0,  0,1,0, 0,-1,0,    //Left side
                                                 1,0,0, 0,1,0,   1,0,1, 0,1,0,   1,1,0, 0,1,0,
                                                 1,1,1, 0,1,0,   1,0,1, 1,0,1,   1,1,0, 1,0,1,     //Right side
                                                 0,0,0, 0,-1,0,  1,0,0, 0,-1,0,  0,0,1, 0,-1,0,
                                                 1,0,1, 0,-1,0,  1,0,0, 0,-1,0,  0,0,1, 0,-1,0,    //Bottom side
                                                 0,1,0, 0,1,0,   1,1,0, 0,1,0,   0,1,1, 0,1,0,
                                                 1,1,1, 0,1,0,   1,1,0, 0,1,0,   0,1,1, 0,1,0,     //Top side
                                            };
        int ind = 0;
        Vertex[] vertices = new Vertex[]{
            /* ---- FRONT ---- */
            new Vertex(new Vector3f(1,1,0), new Vector3f(0,0,1), new Vector2f(this.UV[ind++], this.UV[ind++])),
            new Vertex(new Vector3f(0,0,0), new Vector3f(0,0,1), new Vector2f(this.UV[ind++], this.UV[ind++])),
            new Vertex(new Vector3f(1,0,0), new Vector3f(0,0,1), new Vector2f(this.UV[ind++], this.UV[ind++])),
            
            new Vertex(new Vector3f(1,1,0), new Vector3f(0,0,1), new Vector2f(this.UV[ind++], this.UV[ind++])),
            new Vertex(new Vector3f(0,1,0), new Vector3f(0,0,1), new Vector2f(this.UV[ind++], this.UV[ind++])),
            new Vertex(new Vector3f(0,0,0), new Vector3f(0,0,1), new Vector2f(this.UV[ind++], this.UV[ind++])),
            
            /* ---- BACK ---- */
            new Vertex(new Vector3f(0,1,1), new Vector3f(0,0,-1), new Vector2f(this.UV[ind++], this.UV[ind++])),
            new Vertex(new Vector3f(1,0,1), new Vector3f(0,0,-1), new Vector2f(this.UV[ind++], this.UV[ind++])),
            new Vertex(new Vector3f(0,0,1), new Vector3f(0,0,-1), new Vector2f(this.UV[ind++], this.UV[ind++])),
            
            new Vertex(new Vector3f(0,1,1), new Vector3f(0,0,-1), new Vector2f(this.UV[ind++], this.UV[ind++])),
            new Vertex(new Vector3f(1,1,1), new Vector3f(0,0,-1), new Vector2f(this.UV[ind++], this.UV[ind++])),
            new Vertex(new Vector3f(1,0,1), new Vector3f(0,0,-1), new Vector2f(this.UV[ind++], this.UV[ind++])),
            
            /* ---- LEFT ---- */
            new Vertex(new Vector3f(0,1,0), new Vector3f(-1,0,0), new Vector2f(this.UV[ind++], this.UV[ind++])),
            new Vertex(new Vector3f(0,0,1), new Vector3f(-1,0,0), new Vector2f(this.UV[ind++], this.UV[ind++])),
            new Vertex(new Vector3f(0,0,0), new Vector3f(-1,0,0), new Vector2f(this.UV[ind++], this.UV[ind++])),
            
            new Vertex(new Vector3f(0,1,0), new Vector3f(-1,0,0), new Vector2f(this.UV[ind++], this.UV[ind++])),
            new Vertex(new Vector3f(0,1,1), new Vector3f(-1,0,0), new Vector2f(this.UV[ind++], this.UV[ind++])),
            new Vertex(new Vector3f(0,0,1), new Vector3f(-1,0,0), new Vector2f(this.UV[ind++], this.UV[ind++])),
            
            /* ---- RIGHT ---- */
            new Vertex(new Vector3f(1,1,1), new Vector3f(1,0,0), new Vector2f(this.UV[ind++], this.UV[ind++])),
            new Vertex(new Vector3f(1,0,0), new Vector3f(1,0,0), new Vector2f(this.UV[ind++], this.UV[ind++])),
            new Vertex(new Vector3f(1,0,1), new Vector3f(1,0,0), new Vector2f(this.UV[ind++], this.UV[ind++])),
            
            new Vertex(new Vector3f(1,1,1), new Vector3f(1,0,0), new Vector2f(this.UV[ind++], this.UV[ind++])),
            new Vertex(new Vector3f(1,1,0), new Vector3f(1,0,0), new Vector2f(this.UV[ind++], this.UV[ind++])),
            new Vertex(new Vector3f(1,0,0), new Vector3f(1,0,0), new Vector2f(this.UV[ind++], this.UV[ind++])),
            
            /* ---- BOTTOM ---- */
            new Vertex(new Vector3f(1,0,0), new Vector3f(0,-1,0), new Vector2f(this.UV[ind++], this.UV[ind++])),
            new Vertex(new Vector3f(0,0,1), new Vector3f(0,-1,0), new Vector2f(this.UV[ind++], this.UV[ind++])),
            new Vertex(new Vector3f(1,0,1), new Vector3f(0,-1,0), new Vector2f(this.UV[ind++], this.UV[ind++])),
            
            new Vertex(new Vector3f(1,0,0), new Vector3f(0,-1,0), new Vector2f(this.UV[ind++], this.UV[ind++])),
            new Vertex(new Vector3f(0,0,0), new Vector3f(0,-1,0), new Vector2f(this.UV[ind++], this.UV[ind++])),
            new Vertex(new Vector3f(0,0,1), new Vector3f(0,-1,0), new Vector2f(this.UV[ind++], this.UV[ind++])),
            
            /* ---- TOP ---- */
            new Vertex(new Vector3f(1,1,1), new Vector3f(0,1,0), new Vector2f(this.UV[ind++], this.UV[ind++])),
            new Vertex(new Vector3f(0,1,0), new Vector3f(0,1,0), new Vector2f(this.UV[ind++], this.UV[ind++])),
            new Vertex(new Vector3f(1,1,0), new Vector3f(0,1,0), new Vector2f(this.UV[ind++], this.UV[ind++])),
            
            new Vertex(new Vector3f(1,1,1), new Vector3f(0,1,0), new Vector2f(this.UV[ind++], this.UV[ind++])),
            new Vertex(new Vector3f(0,1,1), new Vector3f(0,1,0), new Vector2f(this.UV[ind++], this.UV[ind++])),
            new Vertex(new Vector3f(0,1,0), new Vector3f(0,1,0), new Vector2f(this.UV[ind++], this.UV[ind++])),
            
        };
        
        //points = applyUV(points);

        this.ovbo = new VBO(new RelativePoints(vertices, 3));
    }
    
    
    public SkyBox(double x, double y, double z, double w, double h, double l){
        this.ox = x;
        this.oy = y;
        this.oz = z;
        
        this.ow = w;
        this.oh = h;
        this.ol = l;
        
        
        reloadVBO();
        reloadModel();
    }
    
    public SkyBox(double x, double y, double z, double w, double h, double l, Texture t){
        this.ox = x;
        this.oy = y;
        this.oz = z;
        
        this.ow = w;
        this.oh = h;
        this.ol = l;
        
        this.setTexture(t);
        
        reloadVBO();
        reloadModel();
    }
    
    
    public SkyBox(double x, double y, double z, double a){
        this(x,y,z, a,a,a);
    }
    
     public SkyBox(double x, double y, double z, double a, Texture t){
        this(x,y,z, a,a,a, t);
    }
}
