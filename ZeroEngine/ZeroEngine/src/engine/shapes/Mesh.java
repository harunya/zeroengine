/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package engine.shapes;

import engine.EBO;
import engine.RelativePoints;
import engine.ShaderProgram;
import engine.Texture;
import engine.TextureAtlas;
import engine.UVs;
import engine.VAO;
import engine.VBO;
import engine.materials.GrassMaterial;
import engine.materials.Material;
import engine.materials.VoidMaterial;
import java.util.ArrayList;
import java.util.List;
import org.joml.Matrix4f;
import org.joml.Vector3f;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;

/**
 *
 * @author mineStile
 */
public class Mesh {
    protected double ox=0,oy=0,oz=0;
    protected double ow=1,oh=1,ol=1;
    protected double or = 0, og = 0, ob = 0, oa = 1;
    protected VBO ovbo ;
    protected RelativePoints points;
    protected Texture texture = null;
    protected Material material = new GrassMaterial();
    protected Matrix4f model = new Matrix4f();
    protected float[] UV = UVs.cube;
    protected EBO oebo;
    protected VAO vao;
    
    protected int VAO, VBO, EBO, EBOlength = -1;
    
    public Material material(){return this.material;};
    public void material(Material mat){this.material = mat;};
    
    public VBO vbo(){return this.ovbo;}
    
    public EBO ebo(){return this.oebo;}
    
    public void setVBO(VBO vbo){this.ovbo = vbo;}
    public void setEBO(EBO ebo){this.oebo = ebo;}
    
    public double x(){return this.ox;}
    public void x(double ofx){this.ox = ofx; reloadModel();}
    
    public double y(){return this.oy;}
    public void y(double ofy){this.oy = ofy; reloadModel();}
    
    public double z(){return this.oz;}
    public void z(double ofz){this.oz = ofz; reloadModel();}
    
    
    
    public Vector3f getVecPos(){
        return new Vector3f((float)this.x(), (float)this.y(), (float)this.z());
    }
    
    
    public double w(){return this.ow;}
    public void w(double ofw){this.ow = ofw; reloadModel();}
    
    public double h(){return this.oh;}
    public void h(double ofh){this.oh = ofh; reloadModel();}
    
    public double l(){return this.ol;}
    public void l(double ofl){this.ol = ofl; reloadModel();}
    
    
    public double r(){return this.or;}
    public void r(double ofx){this.or = ofx;}
    
    public double g(){return this.og;}
    public void g(double ofy){this.og = ofy; }
    
    public double b(){return this.ob;}
    public void b(double ofz){this.ob = ofz;}
    
    public double a(){return this.oa;}
    public void a(double ofw){this.oa = ofw;}
    
    public void setTexture(Texture txt){
        this.texture = txt;
    }
    
    public void setMaterial(Material mat){
        this.material = mat;
    }
    
    public void setUV(float[] uv){
        this.UV = uv;
        reloadVBO();
    }
    
    public void setUV(TextureAtlas ta, String fn, float[] uv){
        this.UV = ta.getUV(fn, uv);
        
        this.setTexture(ta.getTexture());
        
        reloadVBO();
    }
    
    public void reloadVBO(){ 
        
    }
    
    public void optimize(){
        this.vbo().optimize();
    }
    
    protected void reloadModel(){
        model = new Matrix4f();

        model.translate(new Vector3f((float)this.x(), (float)this.y(), (float)this.z()));
        model.scale(new Vector3f((float)this.w(), (float)this.h(), (float)this.l()));
    }
    
    public void loadModel(float[] vertices, int[] indices){
        VAO = GL30.glGenVertexArrays();
        VBO = GL30.glGenBuffers();
        EBO = GL30.glGenBuffers();
        
        EBOlength = indices.length;
        
        GL30.glBindVertexArray(VAO);
        // 2. Копируем наши вершины в буфер для OpenGL
        
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, VBO);
        GL20.glVertexAttribPointer(0, 3, GL11.GL_FLOAT, false, 8*Float.BYTES, 0); // Position
        GL20.glVertexAttribPointer(1, 3, GL11.GL_FLOAT, false, 8*Float.BYTES, 3*Float.BYTES); // Normal
        GL20.glVertexAttribPointer(2, 2, GL11.GL_FLOAT, false, 8*Float.BYTES, 6*Float.BYTES); // UV

        GL20.glEnableVertexAttribArray(0);
        GL20.glEnableVertexAttribArray(1);
        GL20.glEnableVertexAttribArray(2);
        GL15.glBufferData(GL15.GL_ARRAY_BUFFER, vertices, GL15.GL_STATIC_DRAW);
        // 3. Копируем наши индексы в в буфер для OpenGL
        GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, EBO);
        GL15.glBufferData(GL15.GL_ELEMENT_ARRAY_BUFFER, indices, GL15.GL_STATIC_DRAW);
        // 3. Устанавливаем указатели на вершинные атрибуты
        
        // 4. Отвязываем VAO (НЕ EBO)
        GL30.glBindVertexArray(0);
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
        GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, 0);
        
    }
    
    public void draw(ShaderProgram sm){
        if(this.texture != null){
            GL13.glActiveTexture(GL13.GL_TEXTURE0);
            
            //this.texture.toGL(this.UV);
            GL11.glBindTexture(GL11.GL_TEXTURE_2D, this.texture.getId());
        }
        
        
        
        
        sm.sendDataUniform("material.color", new float[]{(float)this.or, (float)this.og, (float)this.ob, (float)this.oa});
        sm.sendDataUniform("material.ambient", this.material.getAmbient());
        sm.sendDataUniform("material.diffuse",  this.material.getDiffuse());
        sm.sendDataUniform("material.specular", this.material.getSpecular());
        sm.sendDataUniform("material.shininess", this.material.getShininess());
        
        sm.sendDataUniform("model", model);
        //this.vbo().draw(GL15.GL_TRIANGLES);
        if(EBOlength != -1){
            GL30.glBindVertexArray(VAO);
            GL30.glDrawElements(GL30.GL_TRIANGLES, EBOlength, GL30.GL_UNSIGNED_INT, 0);
            GL30.glBindVertexArray(0);
        }else this.vbo().draw(GL15.GL_TRIANGLES);
        
        
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, 0);
    }
    
    protected float[] applyUV(float[] points){
        //float[] res = new float[points.length+72];
        float[] res = new float[points.length+points.length/3];
        
        for(int i=0, j=0, v=0;i<res.length;i++, j++){
            res[i] = points[j];
            if((j+1) % 6 == 0){
                res[i+1] = this.UV[v++];
                res[i+2] = this.UV[v++];
                i += 2;
            }
              
        }
        
        return res;
    }
    
    public static float[] vertexToArray(List<Vertex> vs){
        List<Float> res = new ArrayList();
        
        for(Vertex v: vs){
            res.add(v.position.x);
            res.add(v.position.y);
            res.add(v.position.z);
            
            res.add(v.normal.x);
            res.add(v.normal.y);
            res.add(v.normal.z);
            
            if(v.texCoords != null){
                res.add(v.texCoords.x);
                res.add(v.texCoords.y);
            }else{
                res.add(0f);
                res.add(0f);
            }
        }
            
        
        float[] tmp = new float[res.size()];
        for(int i=0;i<tmp.length;i++)
            tmp[i] = res.get(i);
        
        return tmp;
    }
    
    public static int[] listToArray(List<Integer> vs){
        
        int[] tmp = new int[vs.size()];
        for(int i=0;i<tmp.length;i++)
            tmp[i] = vs.get(i);
        
        return tmp;
    }

    void setVertices(List<Vertex> vertices) {
        this.setVBO(new VBO(new RelativePoints(vertexToArray(vertices), 3)));
    }

    void setIndices(List<Integer> indices) {
        this.setEBO(new EBO(listToArray(indices)));
       
    }

    public Matrix4f getModelMatrix() {
        return model;
    }

}
