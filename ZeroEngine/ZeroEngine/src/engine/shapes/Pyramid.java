/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package engine.shapes;

import engine.RelativePoints;
import engine.Texture;
import engine.VBO;

/**
 *
 * @author mineStile
 */
public class Pyramid extends Mesh {
    @Override
    public void reloadVBO(){                     //V1     N1      V2      N2       V3     N3  
        float[] points = new float[]{            0,0,0, 0,-1,0,  1,0,0,    0,-1,0,  0,0,1, 0,-1,0,     //<-
                                                 1,0,1, 0,-1,0,  1,0,0,    0,-1,0,  0,0,1, 0,-1,0,     //Bottom side
                                                 0,0,0,  -1,0,0,  .5f,.5f,.5f, -1,0,0,  1,0,0, -1,0,0, //Left 
                                                 0,0,1,  1,0,0,  .5f,.5f,.5f, 1,0,0,  1,0,1, 1,0,0,    //Right
                                                 0,0,0,  0,0,1,  .5f,.5f,.5f, 0,0,1,  0,0,1, 0,0,1,    //Forward
                                                 1,0,0,  0,0,-1,  .5f,.5f,.5f, 0,0,-1,  1,0,1, 0,0,-1, //Backward
                                            };
        
        points = applyUV(points);

        this.ovbo = new VBO(new RelativePoints(points, 3));
    }
    
    public Pyramid(double x, double y, double z, double w, double h, double l){
        this.ox = x;
        this.oy = y;
        this.oz = z;
        
        this.ow = w;
        this.oh = h;
        this.ol = l;
        
        
        reloadVBO();
        reloadModel();
    }
    
    public Pyramid(double x, double y, double z, double w, double h, double l, Texture t){
        this.ox = x;
        this.oy = y;
        this.oz = z;
        
        this.ow = w;
        this.oh = h;
        this.ol = l;
        
        this.setTexture(t);
        
        reloadVBO();
        reloadModel();
    }
}
