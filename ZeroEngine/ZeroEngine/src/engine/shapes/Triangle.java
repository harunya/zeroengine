/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package engine.shapes;

import engine.RelativePoints;
import engine.VBO;

/**
 *
 * @author mineStile
 */
public class Triangle {
    public static VBO Triangle(float[] vertex){
        VBO vbo = new VBO();
        vbo.set(new RelativePoints(vertex, 3));
        
        return vbo;
    }
}
