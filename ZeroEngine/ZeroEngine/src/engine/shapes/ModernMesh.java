/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package engine.shapes;

import engine.RelativePoints;
import engine.ShaderProgram;
import engine.Texture;
import engine.VBO;
import java.util.ArrayList;
import java.util.List;
import org.joml.Matrix4f;
import org.joml.Vector3f;
import org.lwjgl.assimp.Assimp;
import static org.lwjgl.opengl.GL15.glGenBuffers;
import org.lwjgl.opengl.GL30;
import static org.lwjgl.opengl.GL30.glGenVertexArrays;

/**
 *
 * @author mineStile
 */
public class ModernMesh {
    List<Vertex> vertices;
    List<Texture> textures;
    private VBO vbo;
    private Matrix4f model = new Matrix4f();
    
    public ModernMesh(List<Vertex> vertices, List<Texture> textures){
        this.vertices = vertices;
        this.textures = textures;

        setupMesh();
    }
    private float[] vertexToArray(List<Vertex> vs){
        List<Float> res = new ArrayList();
        
        for(Vertex v: vs){
            res.add(v.position.x);
            res.add(v.position.y);
            res.add(v.position.z);
            
            res.add(v.normal.x);
            res.add(v.normal.y);
            res.add(v.normal.z);
            
            if(v.texCoords != null){
                res.add(v.texCoords.x);
                res.add(v.texCoords.y);
            }else{
                res.add(0f);
                res.add(0f);
            }
        }
            
        
        float[] tmp = new float[res.size()];
        for(int i=0;i<tmp.length;i++)
            tmp[i] = res.get(i);
        
        return tmp;
    }
    private void setupMesh(){
        vbo = new VBO(new RelativePoints(vertexToArray(vertices), 3));
    }  
    
    public void setPosition(Vector3f p){
        model = new Matrix4f();
        model.translate(new Vector3f(p.x, p.y(), p.z()));
    }
    
    public void draw(ShaderProgram sm){
        sm.sendDataUniform("model", model);
        vbo.draw(GL30.GL_TRIANGLES);
    }  
}
