/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package engine.shapes;

import engine.RelativePoints;
import engine.VBO;

/**
 *
 * @author mineStile
 */
public class Shape {
    public static VBO Shape(float x, float y, float z, float w, float h){
        VBO vbo = new VBO();
        vbo.set(new RelativePoints(new float[]{x,y,z, x,y+h,z, x+w,y,z,  x+w,y+h,z ,x,y+h,z, x+w,y,z}, 3));
        
        return vbo;
    }
}
