/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package engine.shapes;

import engine.RelativePoints;
import engine.ShaderProgram;
import engine.Texture;
import engine.UVs;
import engine.VBO;
import java.nio.FloatBuffer;
import java.util.Arrays;
import org.joml.Matrix4f;
import org.joml.Vector2f;
import org.joml.Vector3f;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.system.MemoryStack;

/**
 *
 * @author mineStile
 */
public final class Cube extends Mesh {
    
    
    
    
    @Override
    public void reloadModel(){
        model = new Matrix4f();

        model.translate(new Vector3f((float)this.x(), (float)this.y(), (float)this.z()));
        model.scale(new Vector3f((float)this.w(), (float)this.h(), (float)this.l()));
    }
    
    
    @Override
    public void reloadVBO(){
        int ind = 0;
        Vertex[] vertices = new Vertex[]{
            /* ---- FRONT ---- */
            new Vertex(new Vector3f(1,1,0), new Vector3f(0,0,1), new Vector2f(this.UV[ind++], this.UV[ind++])),
            new Vertex(new Vector3f(1,0,0), new Vector3f(0,0,1), new Vector2f(this.UV[ind++], this.UV[ind++])),
            new Vertex(new Vector3f(0,0,0), new Vector3f(0,0,1), new Vector2f(this.UV[ind++], this.UV[ind++])),
            
            new Vertex(new Vector3f(1,1,0), new Vector3f(0,0,1), new Vector2f(this.UV[ind++], this.UV[ind++])),
            new Vertex(new Vector3f(0,0,0), new Vector3f(0,0,1), new Vector2f(this.UV[ind++], this.UV[ind++])),
            new Vertex(new Vector3f(0,1,0), new Vector3f(0,0,1), new Vector2f(this.UV[ind++], this.UV[ind++])),
            
            /* ---- BACK ---- */
            new Vertex(new Vector3f(0,1,1), new Vector3f(0,0,-1), new Vector2f(this.UV[ind++], this.UV[ind++])),
            new Vertex(new Vector3f(0,0,1), new Vector3f(0,0,-1), new Vector2f(this.UV[ind++], this.UV[ind++])),
            new Vertex(new Vector3f(1,0,1), new Vector3f(0,0,-1), new Vector2f(this.UV[ind++], this.UV[ind++])),
            
            new Vertex(new Vector3f(0,1,1), new Vector3f(0,0,-1), new Vector2f(this.UV[ind++], this.UV[ind++])),
            new Vertex(new Vector3f(1,0,1), new Vector3f(0,0,-1), new Vector2f(this.UV[ind++], this.UV[ind++])),
            new Vertex(new Vector3f(1,1,1), new Vector3f(0,0,-1), new Vector2f(this.UV[ind++], this.UV[ind++])),
            
            /* ---- LEFT ---- */
            new Vertex(new Vector3f(0,1,0), new Vector3f(-1,0,0), new Vector2f(this.UV[ind++], this.UV[ind++])),
            new Vertex(new Vector3f(0,0,0), new Vector3f(-1,0,0), new Vector2f(this.UV[ind++], this.UV[ind++])),
            new Vertex(new Vector3f(0,0,1), new Vector3f(-1,0,0), new Vector2f(this.UV[ind++], this.UV[ind++])),
            
            new Vertex(new Vector3f(0,1,0), new Vector3f(-1,0,0), new Vector2f(this.UV[ind++], this.UV[ind++])),
            new Vertex(new Vector3f(0,0,1), new Vector3f(-1,0,0), new Vector2f(this.UV[ind++], this.UV[ind++])),
            new Vertex(new Vector3f(0,1,1), new Vector3f(-1,0,0), new Vector2f(this.UV[ind++], this.UV[ind++])),
            
            /* ---- RIGHT ---- */
            new Vertex(new Vector3f(1,1,1), new Vector3f(1,0,0), new Vector2f(this.UV[ind++], this.UV[ind++])),
            new Vertex(new Vector3f(1,0,1), new Vector3f(1,0,0), new Vector2f(this.UV[ind++], this.UV[ind++])),
            new Vertex(new Vector3f(1,0,0), new Vector3f(1,0,0), new Vector2f(this.UV[ind++], this.UV[ind++])),
            
            new Vertex(new Vector3f(1,1,1), new Vector3f(1,0,0), new Vector2f(this.UV[ind++], this.UV[ind++])),
            new Vertex(new Vector3f(1,0,0), new Vector3f(1,0,0), new Vector2f(this.UV[ind++], this.UV[ind++])),
            new Vertex(new Vector3f(1,1,0), new Vector3f(1,0,0), new Vector2f(this.UV[ind++], this.UV[ind++])),
            
            /* ---- BOTTOM ---- */
            new Vertex(new Vector3f(1,0,0), new Vector3f(0,-1,0), new Vector2f(this.UV[ind++], this.UV[ind++])),
            new Vertex(new Vector3f(1,0,1), new Vector3f(0,-1,0), new Vector2f(this.UV[ind++], this.UV[ind++])),
            new Vertex(new Vector3f(0,0,1), new Vector3f(0,-1,0), new Vector2f(this.UV[ind++], this.UV[ind++])),
            
            new Vertex(new Vector3f(1,0,0), new Vector3f(0,-1,0), new Vector2f(this.UV[ind++], this.UV[ind++])),
            new Vertex(new Vector3f(0,0,1), new Vector3f(0,-1,0), new Vector2f(this.UV[ind++], this.UV[ind++])),
            new Vertex(new Vector3f(0,0,0), new Vector3f(0,-1,0), new Vector2f(this.UV[ind++], this.UV[ind++])),
            
            /* ---- TOP ---- */
            new Vertex(new Vector3f(1,1,1), new Vector3f(0,1,0), new Vector2f(this.UV[ind++], this.UV[ind++])),
            new Vertex(new Vector3f(1,1,0), new Vector3f(0,1,0), new Vector2f(this.UV[ind++], this.UV[ind++])),
            new Vertex(new Vector3f(0,1,0), new Vector3f(0,1,0), new Vector2f(this.UV[ind++], this.UV[ind++])),
            
            new Vertex(new Vector3f(1,1,1), new Vector3f(0,1,0), new Vector2f(this.UV[ind++], this.UV[ind++])),
            new Vertex(new Vector3f(0,1,0), new Vector3f(0,1,0), new Vector2f(this.UV[ind++], this.UV[ind++])),
            new Vertex(new Vector3f(0,1,1), new Vector3f(0,1,0), new Vector2f(this.UV[ind++], this.UV[ind++])),
            
        };
        
        //points = applyUV(points);

        this.ovbo = new VBO(new RelativePoints(vertices, this.getVecPos(), new Vector3f((float)w(), (float)h(), (float)l()), 3));
    }
    
    
    public Cube(double x, double y, double z, double w, double h, double l){
        this.ox = x;
        this.oy = y;
        this.oz = z;
        
        this.ow = w;
        this.oh = h;
        this.ol = l;
        
        
        reloadVBO();
        reloadModel();
    }
    
    public Cube(double x, double y, double z, double w, double h, double l, Texture t){
        this.ox = x;
        this.oy = y;
        this.oz = z;
        
        this.ow = w;
        this.oh = h;
        this.ol = l;
        
        this.setTexture(t);
        
        reloadVBO();
        reloadModel();
    }
    
    
    public Cube(double x, double y, double z, double a){
        this(x,y,z, a,a,a);
    }
    
     public Cube(double x, double y, double z, double a, Texture t){
        this(x,y,z, a,a,a, t);
    }
    
}
