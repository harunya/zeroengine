/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package engine;

import java.nio.DoubleBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;

/**
 *
 * @author mineStile
 */
public final class VBO {
    private int id = -1;
    private RelativePoints points;
    private int length, vertices = 0;
    private boolean isUnbind = true;
    
    public VBO(){
        this(true);
    }
    public VBO(boolean unbind){
        isUnbind = unbind;
        this.init();
    }
    public VBO(RelativePoints points){
        this(points, true);
    }
    
    public VBO(RelativePoints points, boolean unbind){
        this(unbind);
        this.set(points);
    }
    
    public final void init(){
        this.id = GL15.glGenBuffers();
        //System.out.println(id);
    }
    
    public void free(){
        this.free();
    }
    
    public void set(RelativePoints points){
        this.points = points;
        this.length = points.getPoints().length;
        this.vertices = points.getDimension();
        
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, this.getId());
            GL15.glBufferData(GL15.GL_ARRAY_BUFFER, points.getBuffer(), GL15.GL_STATIC_DRAW);
            GL20.glVertexAttribPointer(0, points.getDimension(), GL11.GL_FLOAT, false, 0, 0);
        GL20.glEnableVertexAttribArray(0);
        
        
            
        if(this.isUnbind)
            GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
    }
    
    public void optimize(){
        //this.points.points = null;
    }
    
    public RelativePoints getPoints(){ return this.points; }
    
    public void draw(int type){
        //System.out.println("Draw VBO with id: "+this.id);
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, this.getId());
        
            GL20.glVertexAttribPointer(0, 3, GL11.GL_FLOAT, false, 8*Float.BYTES, 0); // Position
            GL20.glVertexAttribPointer(1, 3, GL11.GL_FLOAT, false, 8*Float.BYTES, 3*Float.BYTES); // Normal
            GL20.glVertexAttribPointer(2, 2, GL11.GL_FLOAT, false, 8*Float.BYTES, 6*Float.BYTES); // UV

            GL20.glEnableVertexAttribArray(0);
            GL20.glEnableVertexAttribArray(1);
            GL20.glEnableVertexAttribArray(2);
            
            GL11.glDrawArrays(type, 0, this.getPoints().getPoints().length/this.getPoints().getDimension());
            //GL11.glDrawArrays(type, 0, this.length/this.vertices);  
        GL30.glBindVertexArray(0);
    }
    
    public List<RelativePoints> slice(int dim){
        int resCount = (this.points.getPoints().length/3)/dim;
        List<RelativePoints> pts = new ArrayList();
        
        RelativePoints ps = this.getPoints();
        
        for(int i=0;i<resCount;i++){
            float[] p = new float[dim*3];
            //System.out.println(i);
            for(int j=0;j<dim*3;j++){
                //System.out.println(points.getPoints()[i*(dim*3)+j]);
                p[j] = ps.getPoints()[i*(dim*3)+j];
            }
            
            pts.add(new RelativePoints(p, 3));
        }
        
        return pts;
    }
    
    public int getId(){ return this.id; }
}
