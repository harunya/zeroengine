/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package engine;

/**
 *
 * @author mineStile
 */
public class Attrib {
    private int index;
    private String name;
    
    public Attrib(int ind, String nam){
        this.index = ind;
        this.name = nam;
    }
    
    public int getIndex(){ return this.index; }
    
    public String getName(){ return this.name; }
}
