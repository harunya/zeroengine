package engine;

import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author mineStile
 */
public class EBO {
        private int id = -1;
    private RelativePoints points;
    private int length, vertices = 0;
    private IntBuffer indicesBuffer;
    public int buffLength;
    
    
    public EBO(){
        this.init();
    }
    
    public EBO(int id, IntBuffer indicesBuffer){
        this.id =  id;
        this.indicesBuffer = indicesBuffer;
    }
    
    public IntBuffer getBuffer(){
        return indicesBuffer;
    }
    
    private void createBuffer(int[] b){
        this.buffLength = b.length;
        
        indicesBuffer = BufferUtils.createIntBuffer(b.length);
        indicesBuffer.put(b);
        indicesBuffer.flip();
    }
    
    public EBO(byte[] points){
        this.init();
        
        int[] b = new int[points.length];
        
        for(int i=0; i< b.length; i++)
            b[i] = (int)points[i];
        
        createBuffer(b);

        this.set();
    }
    
    public EBO(int[] points){
        this.init();
        
        createBuffer(points);
        
        this.set();
    }
    
    public final void init(){
        this.id = GL15.glGenBuffers();
        //System.out.println(id);
    }
    
    public void set(){
        //this.length = points.getPoints().length;
        //this.vertices = points.getDimension();
        
        GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, this.getId());
            GL15.glBufferData(GL15.GL_ELEMENT_ARRAY_BUFFER, indicesBuffer, GL15.GL_STATIC_DRAW);
            //GL20.glVertexAttribPointer(0, 3, GL11.GL_DOUBLE, false, 0, 0);
            GL20.glEnableVertexAttribArray(0);
        GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, 0);
    }
    
    public RelativePoints getPoints(){ return this.points; }
    
    public void draw(int type){
        //System.out.println("Draw VBO with id: "+this.id);
        GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, this.getId());
        

            
            GL11.glDrawElements(type, buffLength, GL11.GL_UNSIGNED_BYTE, 0);
            //GL11.glDrawArrays(type, 0, this.length/this.vertices);  
        GL30.glBindVertexArray(0);
    }
    
    public int getId(){ return this.id; }
}
