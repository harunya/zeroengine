/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package engine.light;

import engine.ShaderProgram;
import org.joml.Vector3f;

/**
 *
 * @author mineStile
 */
public class DirectionLight {
    private Vector3f ambient;
    private Vector3f specular;
    private Vector3f diffuse;
    private Vector3f direction;
    
    public DirectionLight(Vector3f direction, Vector3f ambient, Vector3f specular, Vector3f diffuse){
        this.ambient = ambient;
        this.diffuse = diffuse;
        this.specular = specular;
        this.direction = direction;
    }
    
    public void sendData(ShaderProgram sm){
        sm.sendDataUniform("dirLight.ambient",  new float[]{ambient.x, ambient.y, ambient.z});
        sm.sendDataUniform("dirLight.diffuse",  new float[]{diffuse.x, diffuse.y, diffuse.z}); // darken the light a bit to fit the scene
        sm.sendDataUniform("dirLight.specular", new float[]{specular.x, specular.y, specular.z}); 
        sm.sendDataUniform("dirLight.direction", new float[]{direction.x, direction.y, direction.z});
    }
    
    public Vector3f getAmbient(){ return this.ambient; }
    public Vector3f getSpecular(){ return this.specular; }
    public Vector3f getDiffuse(){ return this.diffuse; }
    public Vector3f getDirection(){ return this.direction; }
    
    public void setAmbient(Vector3f a){ this.ambient = a; }
    public void setSpecular(Vector3f a){ this.specular = a; }
    public void setDiffuse(Vector3f a){ this.diffuse = a; }
    public void setDirection(Vector3f a){ this.direction = a; }
}
