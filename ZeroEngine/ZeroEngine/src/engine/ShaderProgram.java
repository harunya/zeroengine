/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package engine;

import java.nio.FloatBuffer;
import org.joml.Matrix4f;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;
import org.lwjgl.system.MemoryStack;

/**
 *
 * @author mineStile
 */
public class ShaderProgram {
    private int id;
    
    public ShaderProgram(){
        id = GL20.glCreateProgram();
    }
    
    public void use(){ GL20.glUseProgram(id); }
    
    public void attachShader(Shader sh){
        GL20.glAttachShader(this.id, sh.getId());
        //GL20.glBindAttribLocation(programID, 0, "VertexPosition");
        //GL20.glBindAttribLocation(programID, 1, "VertexColor");
        
        
        GL20.glVertexAttribPointer(0, 3, GL11.GL_FLOAT, false, 8, 0); // Position
        GL20.glVertexAttribPointer(1, 3, GL11.GL_FLOAT, false, 8, 3); // Normal
        //GL20.glVertexAttribPointer(2, 2, GL11.GL_FLOAT, false, 8, 6); // UV
        
        GL20.glEnableVertexAttribArray(0);
        GL20.glEnableVertexAttribArray(1);
       // GL20.glEnableVertexAttribArray(2);
        
        //for(Attrib a: sh.getAttribs()){
        //    //GL20.glBindAttribLocation(this.id, a.getIndex(), a.getName());
        //    
        //    GL20.glEnableVertexAttribArray(a.getIndex());
        //}
        
        
    }
    
    public void link(){
        
        
        GL20.glLinkProgram(id);

        
        
        //GL20.glDetachShader(programID, fsID);

        //GL20.glDeleteShader(fsID);
        if(!GL20.glGetProgramInfoLog(id).equals("")){
            System.out.println(GL20.glGetProgramInfoLog(id));

            System.exit(0);
        }
    }
    
    public int getUniformLocation(String name){
        return GL20.glGetUniformLocation(this.id, name);
    }
    
    public void sendDataUniform(String name, float v){
        GL20.glUniform1f(this.getUniformLocation(name), v);
    }
    
    public void sendDataUniform(String name, float[] v){
        switch(v.length){
            case 1:
                 GL20.glUniform1f(this.getUniformLocation(name), v[0]);
                 break;
            case 2:
                GL20.glUniform2f(this.getUniformLocation(name), v[0], v[1]);
                break;
                
            case 3:
                GL20.glUniform3f(this.getUniformLocation(name), v[0], v[1], v[2]);
                break;
                
            case 4:
                GL20.glUniform4f(this.getUniformLocation(name), v[0], v[1], v[2], v[3]);
                break;
        }
        
    }
    
    public void sendDataUniform(String name, double[] v){
        switch(v.length){
            case 2:
                GL20.glUniform2f(this.getUniformLocation(name), (float)v[0], (float)v[1]);
                break;
                
            case 3:
                GL20.glUniform3f(this.getUniformLocation(name), (float)v[0], (float)v[1], (float)v[2]);
                break;
                
            case 4:
                GL20.glUniform4f(this.getUniformLocation(name), (float)v[0], (float)v[1], (float)v[2], (float)v[3]);
                break;
        }
        
    }
    
    public void sendDataUniform(String name, int[] v){
        switch(v.length){
            case 2:
                GL20.glUniform2i(this.getUniformLocation(name), v[0], v[1]);
                break;
                
            case 3:
                GL20.glUniform3i(this.getUniformLocation(name), v[0], v[1], v[2]);
                break;
                
            case 4:
                GL20.glUniform4i(this.getUniformLocation(name), v[0], v[1], v[2], v[3]);
                break;
        }
        
    }
    
    public void sendDataUniform(String name, Matrix4f v){
        try (MemoryStack stack = MemoryStack.stackPush()){
            FloatBuffer fb = v.get(stack.mallocFloat(16));
            GL20.glUniformMatrix4fv(this.getUniformLocation(name), false, fb);
        }
        
    }
}
