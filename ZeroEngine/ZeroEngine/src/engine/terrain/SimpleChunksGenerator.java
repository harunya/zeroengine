/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package engine.terrain;

import engine.TextureAtlas;
import engine.UVs;
import engine.shapes.Chunk;
import engine.shapes.Cube;
import engine.shapes.Mesh;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author mineStile
 */
public class SimpleChunksGenerator {
    
    public TextureAtlas textureAtlas;
    private SimplexNoise noise;
    private List<Chunk> chunks = new ArrayList<>(); 
    
    public SimpleChunksGenerator(int seed){
        noise = new SimplexNoise(seed);
    }
    
    public List<Chunk> getChunks(){
        return this.chunks;
    }
    
    public void gen(int chunks_x, int chunks_y, int chunks_z, float persistance, float scale){

        chunks = new ArrayList<>(); 
        
        BufferedImage bi = noise.generateHeightmap(256, chunks_y*Chunk.SIZE, persistance, scale, 0, chunks_y*Chunk.SIZE);

        for(int x=0;x<chunks_x;x++){
            for(int y=0;y<chunks_y;y++){
                for(int z=0;z<chunks_z;z++){

                    Chunk chunk = new Chunk(x, y, z);
                    chunk.textureAtlas = textureAtlas;

                    Mesh[] meshes = new Mesh[Chunk.SIZE * Chunk.SIZE * Chunk.SIZE];
                    for(int ox=0, ic=0;ox<Chunk.SIZE;ox++){
                        for(int oy=0;oy<Chunk.SIZE;oy++){
                            for(int oz=0;oz<Chunk.SIZE;oz++,ic++){
                                Color mycolor = new Color(bi.getRGB(ox+x*Chunk.SIZE, oz+z*Chunk.SIZE));

                                int oyf = mycolor.getRed() % 8; // Local y block in chunk
                                int yf = ((int)(mycolor.getRed()/8)); //Global y chunk
                                
                                Mesh m = new Cube(ox*Chunk.BLOCK_SIZE,oy*Chunk.BLOCK_SIZE,oz*Chunk.BLOCK_SIZE, Chunk.BLOCK_SIZE);

                                //TODO: Dynamical adding rule generation
                                if(y*Chunk.SIZE + oy < yf*Chunk.SIZE + oyf) // Stone
                                    m.setUV(textureAtlas, "stone.png", UVs.cube);
                                else if(yf != y)
                                     m = null;
                                else if(oy == oyf){
                                        m.setUV(textureAtlas, "block2.png", UVs.cube);
                                }else
                                    m = null;



                                meshes[ic] = m;




                            }
                        }

                    }
                    chunk.add(meshes);

                    chunks.add(chunk);
                }
            }
        }
    }
}
