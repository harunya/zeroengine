/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package engine;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;

/**
 *
 * @author mineStile
 */
public class VAO {
    private int id;
    private List<VBO> vbos = new ArrayList();
    
    public VAO(){
        this.id = GL30.glGenVertexArrays();
    }
    
    public void bind(){
        GL30.glBindVertexArray(this.id);
    }
    
    public void enable(int ind){
        GL20.glEnableVertexAttribArray(ind);
    }
    
    public void add(VBO vbo){
        this.bind();
            vbo.set(vbo.getPoints());
            vbos.add(vbo);
            this.enable(vbos.size()-1);
        this.unbind();
    }
    
    public void unbind(){
        GL30.glBindVertexArray(0);
    }
    
    public void draw(int type, int ind){
        GL30.glBindVertexArray(this.id);
            
            
            GL11.glDrawArrays(type, 0, vbos.get(ind).getPoints().getPoints().length);
        GL30.glBindVertexArray(0);
    }

    public int getId() {
        return id;
    }
    
}
