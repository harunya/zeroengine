/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package engine.materials;

import engine.Texture;

/**
 *
 * @author mineStile
 */
public class ObsidianMaterial implements Material {

    @Override
    public void Material() {
        
    }

    @Override
    public float[] getAmbient() {
        return new float[]{0.05375f, 0.05f, 0.06625f, 1f};
    }

    @Override
    public float[] getDiffuse() {
        return new float[]{0.18275f, 0.17f, 0.22525f, 1f};
    }

    @Override
    public float[] getSpecular() {
        return new float[]{0.332741f, 0.328634f, 0.346435f, 1f};
    }

    @Override
    public float getShininess() {
        return 0.3f;
    }
    
    @Override
    public Texture getTexture() {
        return new Texture();
    }

    @Override
    public void setTexture(Texture t) {
        
    }
    
}
