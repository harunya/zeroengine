/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package engine.materials;

import engine.Texture;
import org.joml.Vector3f;
import org.joml.Vector4f;

/**
 *
 * @author mineStile
 */
public class CustomMaterial implements Material {
    Vector4f ambient;
    Vector4f diffuse;
    Vector4f specular;
    Texture texture = new Texture();
    float shininess;
    
    public CustomMaterial(Vector4f ambient, Vector4f diffuse, Vector4f specular, float shininess){
        this.ambient = ambient;
        this.diffuse = diffuse;
        this.specular = specular;
        this.shininess = shininess;
    }
    
    public CustomMaterial(Vector3f ambient, Vector3f diffuse, Vector3f specular, float shininess){
        this.ambient = new Vector4f(ambient.x, ambient.y, ambient.z, 1f);
        this.diffuse = new Vector4f(diffuse.x, diffuse.y, diffuse.z, 1f);
        this.specular = new Vector4f(specular.x, specular.y, specular.z, 1f);
        this.shininess = shininess;
    }
    
    
    @Override
    public void Material() {
        
    }

    @Override
    public float[] getAmbient() {
        return new float[]{ambient.x, ambient.y, ambient.z, ambient.w};
    }

    @Override
    public float[] getDiffuse() {
        return new float[]{diffuse.x, diffuse.y, diffuse.z, diffuse.w};
    }

    @Override
    public float[] getSpecular() {
        return new float[]{specular.x, specular.y, specular.z, specular.w};
    }

    @Override
    public float getShininess() {
        return shininess;
    }

    @Override
    public Texture getTexture() {
        return texture;
    }

    @Override
    public void setTexture(Texture t) {
        texture = t;
    }
    
}
