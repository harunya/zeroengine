/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package engine.materials;

import engine.Texture;

/**
 *
 * @author mineStile
 */
public class GrassMaterial implements Material {
    
    @Override
    public void Material() {
        
    }

    @Override
    public float[] getAmbient() {
        return new float[]{0.329412f, 0.223529f, 0.027451f, 1f};
    }

    @Override
    public float[] getDiffuse() {
        return new float[]{0.780392f, 0.568627f, 0.113725f, 1f};
    }

    @Override
    public float[] getSpecular() {
        return new float[]{0.992157f, 0.941176f, 0.807843f, 1f};
    }

    @Override
    public float getShininess() {
        return 27.8974f;
    }

    @Override
    public Texture getTexture() {
        return new Texture();
    }

    @Override
    public void setTexture(Texture t) {
        
    }

}
