/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package engine;

/**
 *
 * @author mineStile
 */
public class Block {
    public int x, y, z = 0;
    private VBO vbo;
    public Block(int x, int y, int z, VBO vbo){
        this.x = x;
        this.y = y;
        this.z = z;
        
        this.vbo = vbo;
    }
    
    public VBO getVBO(){ return this.vbo; }
}
