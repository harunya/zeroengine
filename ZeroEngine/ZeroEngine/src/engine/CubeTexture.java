/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package engine;

import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.embed.swing.SwingFXUtils;
import javax.imageio.ImageIO;

/**
 *
 * @author mineStile
 */
public class CubeTexture {
    private Texture[] textures = new Texture[6];
    
    private static BufferedImage flipImageHorizontal(BufferedImage image){
        // Flip the image horizontally
        AffineTransform tx = AffineTransform.getScaleInstance(-1, 1);
        tx.translate(-image.getWidth(null), 0);
        AffineTransformOp op = new AffineTransformOp(tx, AffineTransformOp.TYPE_NEAREST_NEIGHBOR);
        return op.filter(image, null);
    }
    
    private static BufferedImage flipImageVertical(BufferedImage image){
        // Flip the image vertically
        AffineTransform tx = AffineTransform.getScaleInstance(1, -1);
        tx.translate(0, -image.getHeight(null));
        AffineTransformOp op = new AffineTransformOp(tx, AffineTransformOp.TYPE_NEAREST_NEIGHBOR);
        return op.filter(image, null);
    }
    
    private static BufferedImage rotateImage(BufferedImage bufferedImage, float angle){
        float radians = (float) Math.toRadians(angle);
        
        AffineTransform transform = new AffineTransform();
        transform.rotate(radians, bufferedImage.getWidth()/2, bufferedImage.getHeight()/2);
        AffineTransformOp op = new AffineTransformOp(transform, AffineTransformOp.TYPE_BILINEAR);
        return op.filter(bufferedImage, null);
    }
    
    public void loadFromArrayImage(String fn, int size, float[] texCoords){
        try {
            BufferedImage dest = ImageIO.read(new File(fn));
            
            //size -= 1;
            
            this.textures[0] = Texture.createTexture(new Image(dest.getSubimage(size, size, size, size)));
            this.textures[1] = Texture.createTexture(new Image(dest.getSubimage(size*3, size, size, size)));
            this.textures[2] = Texture.createTexture(new Image(dest.getSubimage(0, size, size, size)));
            this.textures[3] = Texture.createTexture(new Image(dest.getSubimage(size*2, size, size, size)));
            this.textures[4] = Texture.createTexture(new Image(dest.getSubimage(size, size*2, size, size)));
            this.textures[5] = Texture.createTexture(new Image(dest.getSubimage(size, 0, size, size)));
            
            for(int i=0;i<6;i++)
                this.textures[i].toGL(texCoords);
            
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    public Texture getTexture(int i){
        //if(this.textures.length <= i)
        //    return null;
        
        return this.textures[i];
    }
}
