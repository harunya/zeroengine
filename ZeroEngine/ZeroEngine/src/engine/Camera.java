/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package engine;

import static java.lang.System.gc;
import java.nio.ByteBuffer;
import java.util.Arrays;
import org.joml.Matrix4f;
import org.joml.Vector3f;
import org.lwjgl.glfw.*;
import static org.lwjgl.glfw.GLFW.*;
import org.lwjgl.opengl.GL11;

/**
 *
 * @author mineStile
 */
public class Camera {
    public double angleX, angleY, posX = 0, posY = 0, posZ = 0;
    Window window;
    private float lastX, lastY, xOffset, yOffset, yaw, pitch = 0;
    private boolean firstMouse = true;
    private Vector3f up = new Vector3f(0.0f, 1.0f, 0.0f);
    private Vector3f cameraPos = new Vector3f(0,0,0), cameraFront = new Vector3f(0,0,0), cameraDirection = new Vector3f(0,0,0), cameraRight = new Vector3f(0,0,0), cameraUp = new Vector3f(0,0,0);
    private Matrix4f view;
    public float cameraSpeed = 0.3f;
    public boolean enabled = false;
    
    public Camera(Window win){
        this.window = win;
        
        glfwSetInputMode(window.getID(), GLFW_CURSOR, GLFW_CURSOR_DISABLED);
        glfwSetCursorPosCallback(window.getID(), (window1, xpos, ypos) -> {
            
            
            if(firstMouse) { // Это чтобы курсор не скакал при первом движении
                lastX = (float) xpos;
                lastY = (float) ypos;
                firstMouse = false;
            }
            
            //System.out.println(xpos+" "+ypos);
            
            float xoffset = (float) (xpos - lastX);
            float yoffset = (float) (lastY - ypos);
 
            lastX = (float) xpos;
            lastY = (float) ypos;
 
            float sensitivity = 0.07f;
            xoffset *= sensitivity;
            yoffset *= sensitivity;
            pitch += yoffset;
            yaw   += xoffset;
            if(pitch > 89.0f) pitch =  89.0f; // Чтобы мы не могли разворачиваться на тучу градусов
            if(pitch < -89.0f) pitch = -89.0f;
            
            Vector3f front = new Vector3f((float) ( Math.cos(Math.toRadians(yaw)) * Math.cos(Math.toRadians(pitch)) ),
                                      (float) Math.sin(Math.toRadians(pitch)),
                                      (float) ( Math.sin(Math.toRadians(yaw)) * Math.cos(Math.toRadians(pitch)) ));
            
            this.cameraFront = front.normalize();
            
            
            
            
            
            
            
            
            
            //System.out.println(getViewMatrix());
        });
    }
    
    public Matrix4f getViewMatrix(){
        return view;
    }
    
    public void update(){
        cameraDirection = cameraPos.sub(this.getVec(), new Vector3f());

        cameraRight = up.cross(cameraDirection, new Vector3f());
        cameraRight = cameraRight.normalize();

        cameraUp = cameraDirection.cross(cameraRight, new Vector3f());
        
        this.view = (new Matrix4f()).lookAt(this.getPosVec(), 
                                cameraPos.add(getVec(), new Vector3f()), 
                                cameraUp);
        
        if(!enabled) return;
        
        //glfwSetInputMode(window.getID(), GLFW_CURSOR, GLFW_CURSOR_DISABLED);
        
        double[] x = new double[1];
        double[] y = new double[1];
        
        glfwGetCursorPos(window.getID(), x, y);
        
        double xt = window.getWidth()/2;
        double yt = window.getHeight()/2;
        
        if(firstMouse){
            lastX = (float)x[0];
            lastY = (float)y[0];
            firstMouse = false;
        }
        
        
        
        float xoffset = (float)x[0] - lastX;
        float yoffset = lastY - (float)y[0]; 
        lastX = (float)xt;
        lastY = (float)yt;

        float sensitivity = 0.3f;
        
        xoffset *= sensitivity;
        yoffset *= sensitivity;

        yaw   += xoffset;
        pitch += yoffset;

        if(pitch > 89.0f)
            pitch = 89.0f;
        if(pitch < -89.0f)
            pitch = -89.0f;

        
        
        
        cameraDirection = cameraPos.sub(this.getVec(), cameraDirection);
        
        up.cross(cameraDirection, cameraRight);
        cameraRight.normalize();
        
        cameraUp = cameraDirection.cross(cameraRight);
        
        glfwSetCursorPos(window.getID(), xt, yt);
    }
    
    public void moveForward() {     
        cameraPos.add(new Vector3f(this.getVec()).mul(cameraSpeed));
    }
    
    public void moveBackward() {
        
        cameraPos.sub(new Vector3f(this.getVec()).mul(cameraSpeed));
    }
    
    public void moveLeft() {
       cameraPos.sub(this.getVec().cross(cameraUp, new Vector3f()).normalize(new Vector3f()).mul(cameraSpeed, new Vector3f()));
    }
    
    public void moveRight() {
        cameraPos.add(this.getVec().cross(cameraUp, new Vector3f()).normalize(new Vector3f()).mul(cameraSpeed, new Vector3f()));
    }
    
    
    public Vector3f getVec(){
        return cameraFront;
    }
    
    public Vector3f getPosVec(){
        return cameraPos;
    }
    
    

}
