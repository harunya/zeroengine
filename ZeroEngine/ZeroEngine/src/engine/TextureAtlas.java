/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package engine;

import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import org.joml.Vector2f;
import org.joml.Vector2i;
import org.joml.Vector4f;
import org.joml.Vector4i;

/**
 *
 * @author mineStile
 */
public class TextureAtlas {
    private Map<String, Vector4i> descr = new HashMap();
    private Texture t;
    private BufferedImage atlas;
    
    public Texture getTexture(){
        return t;
    }
    
    private boolean isCollision(Vector4i a, Vector4i b){
        //if(Math.max(a.x, a.z) < Math.min(a.x, a.z) || Math.min(a.x, a.z) > Math.max(a.x, a.z)) return false;
        //if(Math.max(a.y, a.w) < Math.min(a.y, a.w) || Math.min(a.y, a.w)  > Math.max(a.y, a.w)) return false;
        
        return (a.x < b.x + b.z && a.x + a.z > b.x && a.y < b.y + b.w && a.y + a.w > b.y);
    }
    
    
    
    private Vector2i getFreePos(BufferedImage img, BufferedImage im, List<Vector4i> objs){
        for(int x=0; x < im.getWidth()-img.getWidth(); x++){
            for(int y=0; y < im.getHeight()-img.getHeight(); y++){
                //int pixel = im.getRGB(x, y);
                
                if(objs.size() == 0)
                    return new Vector2i(x, y);
                
                int c = 0;
                for(Vector4i obj: objs){
                    if(isCollision(new Vector4i(x,y, img.getWidth(), img.getHeight()), obj))
                        c++;
                }
                
                if(c == 0)
                    return new Vector2i(x, y);
            }
        }
        
        return new Vector2i(-1, -1);
    }
    
    public void sendTextureToOGL(){
        t = Texture.createTexture(new Image(atlas));
    }
    
    public void gen(String path){
       System.out.println("Generation TextureAtlas from directory \""+path+"\" started!");
        
        File folder = new File(path);
        
        int width = 0, height = 0;
        
        
        for (final File fileEntry : folder.listFiles()) {
            if (!fileEntry.isDirectory()) {
                String fn = fileEntry.getName();
                
                try {
                    BufferedImage img = ImageIO.read(new File(fileEntry.getAbsolutePath()));
                    
                    width += img.getWidth();
                    
                    if(height < img.getHeight())
                        height = img.getHeight();
                    
                } catch (Exception e) {
                    System.err.println("Error load "+fn+":"+e);
                }
            }
        }
        
        
        
        atlas = new BufferedImage(1024 * 8, 1024 * 4, BufferedImage.TYPE_INT_ARGB);
        
        List<Vector4i> objs = new ArrayList<>();
        for (final File fileEntry : folder.listFiles()) {
            if (!fileEntry.isDirectory()) {
                String fn = fileEntry.getName();
                
                try {
                    BufferedImage img = ImageIO.read(new File(fileEntry.getAbsolutePath()));
                    
                    
                    
                    Vector2i p = getFreePos(img, atlas, objs);
                    
                    Vector4i imgv = new Vector4i(p.x, p.y, img.getWidth(), img.getHeight());
                    objs.add(imgv);
                    
                    System.out.println("Added "+fn+" in ("+imgv.x+" "+imgv.y+")");
                    //System.out.println(" / "+imgv.z+" "+imgv.w);
                    
                    Graphics2D g2d = atlas.createGraphics();
                    g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1f));
                    g2d.drawImage(img, imgv.x, imgv.y, null);
                    g2d.dispose();
                    
                    this.descr.put(fn, imgv);
                    
                    
                } catch (Exception e) {
                    System.err.println("Error load "+fileEntry.getAbsolutePath()+" :"+e);
                }
            }
        }
        
        int maxX = 0, maxY = 0;
        for(Vector4i obj: objs){
            if(maxX < obj.x + obj.z) maxX = obj.x + obj.z;
            if(maxY < obj.y + obj.w) maxY = obj.y + obj.w;
        }
        
        BufferedImage newAtlas = new BufferedImage(maxX, maxY, BufferedImage.TYPE_INT_ARGB);

        Graphics2D g2d = newAtlas.createGraphics();
        g2d.drawImage(atlas, 0, 0, null);
        g2d.dispose();
        
        atlas = newAtlas;
        newAtlas = null;

        
        System.out.println("Process generation TextureAtlas ended. Added "+descr.size()+" textures.");
        
        
        
        
        //try {
        //    ImageIO.write(atlas, "png", new File("atlas.png"));
        //} catch (IOException ex) {
        //    Logger.getLogger(TextureAtlas.class.getName()).log(Level.SEVERE, null, ex);
        //}
        
         
        
        
    }
    
    public float[] getUV(String fn, float[] UV){
        Vector4i part = this.descr.get(fn);
        
        return UVs.getPreparedUV(UV, new Vector2f(atlas.getWidth(), atlas.getHeight()), new Vector4f(part.x, part.y, part.z, part.w));
    }
}
