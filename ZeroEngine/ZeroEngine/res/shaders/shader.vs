#version 330 core


uniform vec4 myUniform;
uniform mat4 model;
uniform mat4 view;
uniform mat4 projection; 
uniform vec3 offset;
uniform float density;

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 texCoords;

//out vec3 ourColor;
out vec2 TexCoords;
out vec3 Normal;
out vec3 FragPos;  
out float visibility;



const float gradient = 1.5f;

void main(){     
    
    FragPos = vec3(model * vec4(position, 1.0f));
    Normal = mat3(transpose(inverse(model))) * normal;
    TexCoords = texCoords;

    vec4 posRelToCam = view*model*vec4(position, 1.0f);
    gl_Position = projection*posRelToCam;
    
    //gl_Position = vec4(position+offset, 1.0f);

    //if(myUniform.w != 0)
    //    gl_Position = vec4(rotate(gl_Position, myUniform.xyz, myUniform.w), 1.0f);


    /* FOG */
    float distance = length(posRelToCam.xyz);
    visibility = exp(-pow((distance * density), gradient));
    visibility = clamp(visibility, 0f, 1f);
}
