#version 330 core
out vec4 FragColor;

struct Material {
    vec4 ambient;
    vec4 diffuse;
    vec4 specular;
    vec4 color;

    float shininess;
}; 

struct DirLight {
    vec3 direction;
  
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};  
struct PointLight {    
    vec3 position;
    
    float constant;
    float linear;
    float quadratic;  

    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};  
struct SpotLight {    
    vec3 position;
    vec3  direction;
    
    float cutOff; 
    float outerCutOff;
}; 


#define NR_POINT_LIGHTS 50
#define NR_SPOT_LIGHTS 50

//in vec3 ourColor;
in vec3 FragPos;
in vec2 TexCoords;
in vec3 Normal;
in float visibility;


uniform float POINT_LIGHTS;
uniform float SPOT_LIGHTS;

uniform PointLight pointLights[NR_POINT_LIGHTS];
uniform SpotLight spotLights[NR_SPOT_LIGHTS];
uniform DirLight dirLight;

uniform Material material;
uniform vec3 lightColor;
uniform vec4 lightVector;
uniform vec4 ambientConfig;
uniform sampler2D ourTexture;
uniform vec3 viewPos;


// function prototypes
vec4 CalcDirLight(DirLight light, vec3 normal, vec3 viewDir);
vec4 CalcPointLight(PointLight light, vec3 normal, vec3 fragPos, vec3 viewDir);
vec4 CalcSpotLight(vec4 res, SpotLight light, vec3 normal, vec3 fragPos, vec3 viewDir);


float specularStrength = 0.5f;

void main(){
    // свойства
    vec3 norm = normalize(Normal);
    vec3 viewDir = normalize(viewPos - FragPos);

    // фаза 1: Направленный источник освещения
    vec4 result = CalcDirLight(dirLight, norm, viewDir);

    result += ambientConfig.w;
    
    // фаза 2: Точечные источники
    for(int i = 0; i < POINT_LIGHTS; i++)
        result += CalcPointLight(pointLights[i], norm, FragPos, viewDir);    
        
    // фаза 3: фонарик
    //for(int j = 0; j < SPOT_LIGHTS; j++)
    //    result += CalcSpotLight(result, spotLights[j], norm, FragPos, viewDir);    
   
    vec4 texColor;
    if(texture(ourTexture, TexCoords).xyz == vec3(0)){
        texColor = material.color*result;
    }else{
        texColor = texture(ourTexture, TexCoords)*result;
    }
    float gamma = 2.2;
    //vec3 diffuseColor = pow(texture(diffuse, texCoords).rgb, vec3(gamma));
    
    FragColor = vec4(pow(texColor.rgb, vec3(gamma)), texColor.w); 
    FragColor = mix(vec4(ambientConfig.xyz, 1f), FragColor, visibility);
    //FragColor = vec4(fxaa(FragColor, gl_FragCoord.xy, vec2(800f, 600f), ).xyz, FragColor.w);
    
}

vec4 CalcDirLight(DirLight light, vec3 normal, vec3 viewDir){
    //vec3 lightDir   = normalize(light.position - FragPos);
    //vec3 viewDir    = normalize(viewPos - FragPos);
    //vec3 halfwayDir = normalize(lightDir + viewDir);
    vec3 lightDir = normalize(-light.direction);
    // диффузное освещение
    float diff = max(dot(normal, lightDir), 0.0);
    // освещение зеркальных бликов
    vec3 reflectDir = reflect(-lightDir, normal);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
    // комбинируем результаты
    vec4 ambient  = vec4(light.ambient, 1f)  * material.diffuse;
    vec4 diffuse  = vec4(light.diffuse, 1f)  * diff * material.diffuse;
    vec4 specular = vec4(light.specular, 1f) * spec * material.specular;
    return (ambient + diffuse + specular);
} 

vec4 CalcPointLight(PointLight light, vec3 normal, vec3 fragPos, vec3 viewDir){
    vec3 lightDir = normalize(light.position - fragPos);
    vec3 halfwayDir = normalize(lightDir + viewDir);
    // диффузное освещение
    float diff = max(dot(normal, lightDir), 0.0);
    // освещение зеркальных бликов
    vec3 reflectDir = reflect(-lightDir, normal);
    float spec = pow(max(dot(viewDir, halfwayDir), 0.0), material.shininess);
    // затухание
    float distance    = length(light.position - fragPos);
    float attenuation = 1.0 / distance;
    //float attenuation = 1.0 / (light.constant + light.linear * distance + 
  //                 light.quadratic * (distance * distance));    
    // комбинируем результаты
    vec4 ambient  = vec4(light.ambient, 1f)  * material.diffuse;
    vec4 diffuse  = vec4(light.diffuse, 1f)  * diff * material.diffuse;
    vec4 specular = vec4(light.specular, 1f) * spec * material.specular;
    ambient  *= attenuation;
    diffuse  *= attenuation;
    specular *= attenuation;
    return (ambient + diffuse + specular);
} 

vec4 CalcSpotLight(vec4 res, SpotLight light, vec3 normal, vec3 fragPos, vec3 viewDir){
    float theta     = dot(light.direction, normalize(-light.direction));
    float epsilon   = light.cutOff - light.outerCutOff;
    float intensity = clamp((theta - light.outerCutOff) / epsilon, 0.0, 1.0);    
        
    return vec4(intensity,intensity,intensity, 1f);
} 
