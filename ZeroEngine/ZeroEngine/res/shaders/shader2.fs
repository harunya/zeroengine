#version 330 core
out vec4 FragColor;
  
//in vec3 ourColor;
in vec3 FragPos;
in vec2 outTexCoords;
in float visibility;



uniform sampler2D ourTexture;
uniform vec4 ambientConfig;


void main(){
    vec3 ambient = ambientConfig.w * ambientConfig.xyz;

    FragColor = vec4(texture(ourTexture, outTexCoords).xyz*ambient, 1.0f);
    FragColor = mix(vec4(ambientConfig.xyz, 1f), FragColor, visibility);

}
