#version 330 core

uniform vec4 myUniform;
uniform mat4 model;
uniform mat4 view;
uniform mat4 projection; 
uniform vec3 offset;
uniform float density;

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 texCoords;

out vec2 outTexCoords;
out float visibility;


const float gradient = 1.5f;

void main(){     
    outTexCoords = texCoords;

    vec4 posRelToCam = view*model*vec4(position, 1.0f);
    gl_Position = projection*posRelToCam;
    
    /* FOG */
    float distance = length(posRelToCam.xyz);
    visibility = exp(-pow((distance * density), gradient));
    visibility = clamp(visibility, 0f, 1f);
}

